package com.topOne.mr.mapper;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.topOne.bean.Flow;

public class FlowCountMapper extends Mapper<LongWritable, Text, Text, Flow> {
	
	@Override
	protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Flow>.Context context)
			throws IOException, InterruptedException {
		String line = value.toString();
		String phoneNumber = line.split(",")[0];
		String upFlow = line.split(",")[1];
		String downFlow = line.split(",")[2];
		Flow flow = new Flow(Long.parseLong(upFlow), Long.parseLong(downFlow));
		context.write(new Text(phoneNumber), flow);
	}

}
