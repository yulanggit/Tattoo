package com.topOne.mr.master;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import com.topOne.mr.mapper.WordCountMapper;
import com.topOne.mr.reducer.WordCountReducer;
import com.topOne.util.PropertiesUtil;

public class WordCountMaster {

	public boolean WordCount(String inputPath,String outputPath) throws Exception {
		// 初始化配置
		Configuration conf = new Configuration();
		// 设置fs.defaultFS参数
		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
		String hostName = propertiesUtil.readPropertyByKey("hostName");
		conf.set("fs.defaultFS", "hdfs://"+ hostName +":8020/");
		// 初始化job参数，指定job名称
		Job job = Job.getInstance(conf, "wordCount");
		// 设置运行job的类
		job.setJarByClass(WordCountMaster.class);
		// 设置Mapper类
		job.setMapperClass(WordCountMapper.class);
		// 设置Reducer类
		job.setReducerClass(WordCountReducer.class);
		// 设置Map的输出数据类型
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		// 设置Reducer的输出数据类型
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		// 设置输入的路径 -> 可以指定为某一路径或具体的文件
		FileInputFormat.setInputPaths(job, new Path(inputPath));
		// 设置输出的路径 -> 最后一级路径自动生成，不会自动覆盖，需要手动修改
		FileOutputFormat.setOutputPath(job, new Path(outputPath));
		// 提交job,将执行结果返回
		return job.waitForCompletion(true);
	}

}
