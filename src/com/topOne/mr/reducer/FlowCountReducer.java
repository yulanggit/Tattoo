package com.topOne.mr.reducer;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import com.topOne.bean.Flow;


public class FlowCountReducer extends Reducer<Text, Flow, Text, Flow> {
	
	@Override
	protected void reduce(Text key, Iterable<Flow> values, Reducer<Text, Flow, Text, Flow>.Context context)
			throws IOException, InterruptedException {
		long upFlow = 0;
		long downFlow = 0;
		for (Flow flow : values) {
			upFlow += flow.getUpFlow();
			downFlow += flow.getDownFlow();
		}
		Flow flow = new Flow(upFlow, downFlow);
		context.write(key, flow);
	}

}
