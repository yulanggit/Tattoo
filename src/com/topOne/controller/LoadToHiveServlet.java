package com.topOne.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.hadoop.hdfs.qjournal.protocol.QJournalProtocolProtos.NewEpochRequestProto;

import com.sun.jersey.core.util.StringIgnoreCaseKeyComparator;
import com.topOne.util.DBUtil;
import com.topOne.util.HDFSUtil;
import com.topOne.util.HiveUtil;
import com.topOne.util.PropertiesUtil;

/**
 * Servlet implementation class LoadToHiveServlet
 */
@WebServlet("/LoadToHiveServlet")
@MultipartConfig
public class LoadToHiveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoadToHiveServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 设置编码
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html ; charset=UTF-8");

		// 所需工具类
		DBUtil dbUtil = new DBUtil();

		// 传入的用户名
		// 用户所属的hive数据库

		String id = request.getParameter("id");
		System.out.println(id);
		ResultSet rs = null;

		// 接受参数
		// 数据源名称
		String sourcename = request.getParameter("sourcename");
		System.out.println(sourcename);

		// 数据表名称
		String tableName = request.getParameter("tableName");
		System.out.println(tableName);
		// 是否自动读取字段信息
		boolean isAuto = request.getParameter("auto") == null ? false : true;
		System.out.println(isAuto);
		// 是否覆盖导入
		boolean isOverwrite = request.getParameter("overwrite") == null ? false : true;
		System.out.println(isOverwrite);
		// 列分隔符
		String format = request.getParameter("format");
		System.out.println(format);
		String columnName = "";

		String[] columnTypes = request.getParameterValues("columnType");
		for (String columnType : columnTypes) {
			System.out.println(columnType);
		}

		// 选择分组
		String groupid = request.getParameter("group_name");
		System.out.println(groupid);

		String uploadSpace = request.getParameter("userName");

		// 使用Part对象接受文件
		Part part = request.getPart("data");
		// 取出文件名(如果需要)
		String path = "D:\\";
		// 可以使用自定义的名称，也可以使用UUID
		String fileName = UUID.randomUUID().toString();

		String filepath = path + File.separator + fileName;
		// 使用write方法向路径中写入文件
		part.write(filepath);
		String result111 = "";
		String createTable = "create table " + uploadSpace + "." + tableName + "(";
		// 判断是否获取字段信息
		if (!isAuto) {
			String[] columnNames = request.getParameterValues("columnName");

			// for (String columName : columnNames) {
			// columnName += columName +"\""+format +"\"";
			// }
			for (int i = 0; i < columnNames.length; i++) {
				result111 += columnNames[i] + " " + columnTypes[i] + ",";
			}
			result111 = result111.substring(0, result111.length() - 1);
			createTable = "create table " + uploadSpace + "." + tableName + "(" + result111
					+ ") row format delimited fields terminated by '" + format + "'";

		}

		else {
			Reader reader = new FileReader(new File(filepath));
			BufferedReader bf = new BufferedReader(reader);
			// String tableInfo = bf.readLine();
			columnName = bf.readLine();
			bf.close();
			reader.close();

			for (int i = 0; i < columnTypes.length; i++) {
				createTable += columnName.split(format)[i] + " " + columnTypes[i] + ",";
			}
			createTable = createTable.substring(0, createTable.length() - 1);
			createTable += ") row format delimited fields terminated by '" + format + "'";
		}

		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
		String hostName = propertiesUtil.readPropertyByKey("hostName");
		HDFSUtil hdfsUtil = new HDFSUtil(hostName);
		hdfsUtil.upLoad(true, true, new String[] { filepath }, "/Common");

		// createTable += ") row format delimited fields terminated by '" + format
		// + "' tblproperties(\"skip.header.line.count\"=\"1\")";

		System.out.println(createTable);
		HiveUtil hiveUtil = new HiveUtil();
		boolean result = hiveUtil.execute(createTable);
		System.out.println(result);

		String loadToHive = "load data inpath '/Common/" + fileName + "' " + (isOverwrite ? "overwrite" : "")
				+ " into table " + uploadSpace + "." + tableName;
		System.out.println(loadToHive);
		boolean result2 = hiveUtil.execute(loadToHive);
		if (result2) {
			System.out.println(tableName);
			hiveUtil.changeDatabase(uploadSpace);
			List<String> columNames = hiveUtil.getTableInfo(tableName);
			String columName = "";
			for (String string : columNames) {
				columName += string + ",";
			}
			columName.substring(0, columName.length() - 1);
			System.out.println(columName);
			String sql = "insert into data_source(source_name,source_type,hive_table,table_column,user_id,group_id) values(?,'hive',?,?,?,?)";
			dbUtil.executeUpdate(sql, new Object[] { sourcename, tableName, columName, id, groupid });
			response.sendRedirect("/Tattoo/LoadToHive.jsp?result2=" + result2);
		} else {
			response.sendRedirect("/Tattoo/LoadToHive.jsp?result2=" + result2);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
