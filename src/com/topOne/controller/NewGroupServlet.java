package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.topOne.JsonBackData.JsonBackData;
import com.topOne.util.DBUtil;
import com.topOne.util.HDFSUtil;
import com.topOne.util.PropertiesUtil;

import net.sf.json.JSONObject;

/**
 * Servlet implementation class NewGroupServlet
 */
@WebServlet("/NewGroupServlet")
public class NewGroupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewGroupServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String id = request.getParameter("id");
		String userName = request.getParameter("userName");
		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
		DBUtil dbUtil = new DBUtil();
		int resultInt = -1;
		String path = "";
		String sql = "";
		String group_id = "";
		String group_name = "";
		String method = request.getParameter("method");
		switch (method) {
		case "newGroup":
			String newgroupName = request.getParameter("newgroupName");
			sql = "insert into source_group(group_name,user_id) values(?,?)";
			resultInt = dbUtil.executeUpdate(sql, new Object[] { newgroupName, id });
			if (resultInt != -1 || resultInt != 0) {
				out.print(JSONObject.fromObject(JsonBackData.success("添加成功")));
			} else {
				out.print(JSONObject.fromObject(JsonBackData.fail("添加失败")));
			}
			out.flush();
			out.close();
			break;

		case "deleteGroup":
			group_id = request.getParameter("group_id");
			sql = "delete from source_group where id = " + group_id;
			System.out.println(sql);
			resultInt = dbUtil.executeUpdate(sql, null);
			if (resultInt != -1 || resultInt != 0) {
				out.print(JSONObject.fromObject(JsonBackData.success("删除成功")));
			} else {
				out.print(JSONObject.fromObject(JsonBackData.fail("删除失败")));
			}
			out.flush();
			out.close();
			break;

		case "updateGroup":
			group_id = request.getParameter("group_id");
			String new_group_name = request.getParameter("UpdategroupName");
			sql = "update source_group set group_name = '" + new_group_name + "' where id = " + group_id;
			System.out.println(sql);
			resultInt = dbUtil.executeUpdate(sql, null);
			if (resultInt != -1 || resultInt != 0) {
				out.print(JSONObject.fromObject(JsonBackData.success("修改成功")));
			} else {
				out.print(JSONObject.fromObject(JsonBackData.fail("修改失败")));
			}
			out.flush();
			out.close();
			break;

		default:
			break;
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
