package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.topOne.JsonBackData.JsonBackData;
import com.topOne.service.CountAnalysis;
import com.topOne.util.DBUtil;
import com.topOne.util.HiveUtil;
import com.topOne.util.PropertiesUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class CountHQLServlet
 */
@WebServlet("/CountHQLServlet")
public class CountHQLServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CountHQLServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain ; charset=UTF-8");
		PrintWriter out = response.getWriter();
		DBUtil dbUtil = new DBUtil();
		String method = request.getParameter("method");
		
		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties"); 
		String hostName =  propertiesUtil.readPropertyByKey("hostName");
		HiveUtil hiveUtil = new HiveUtil();
		//获取当前登录用户的user_id、user_name
		String user_id = request.getParameter("user_id");
		System.out.println("user_id=" + user_id);
		//String user_name = (new CountAnalysis()).userName(user_id);
		String user_name = request.getParameter("user_name");
		System.out.println(user_name);
		
		switch(method) {
		case "HQLcalculation":
			String sql = request.getParameter("sql");
			String resultTable = request.getParameter("resultTable");
			String database1 = request.getParameter("database1");
			String commom = request.getParameter("commom");
			System.out.println(sql+ "\n" + resultTable+ "\n" + database1+ "\n" +commom);
			//处理计算 --> 把计算结果返回HQL --js
		
			break;
		case "showTable1":
			//获取用户指定表的所有信息  --字段--内容
			String id = request.getParameter("tableId");
			System.out.println("id="+id);
			String sql1 = "select hive_table from data_source where id = ?";
			ResultSet rs =dbUtil.executeQuery(sql1, new Object[] {id});
			String hive_table = "";
			List<String> list = new ArrayList<>();
			List<Object> list2 = new ArrayList<>();
			//获取到字段信息
			try {
				while(rs.next()) {
					hive_table = rs.getString(1);
					hiveUtil.changeDatabase(user_name);
					list = hiveUtil.getTableInfo(hive_table);
					System.out.println("hive_table"+hive_table);
				}
				for(String temp:list) {
					list2.add(temp.split("\t")[0]);
				}
				System.out.println("获取到字段信息"+list);
				System.out.println(list2);
			} catch (SQLException e) {
				e.printStackTrace();
				list2 = null;
			}
			out.print(JSONObject.fromObject(JsonBackData.successRtnObject(list2)));
			dbUtil.close();
			break;
		case "showTable2":
			//获取表内容
			String id1 = request.getParameter("tableId");
			System.out.println("id="+id1);
			String sql2 = "select hive_table from data_source where id = ?";
			ResultSet rs1 =dbUtil.executeQuery(sql2, new Object[] {id1});
			String hive_table1 = "";
			List<String> list3 = new ArrayList<>();
			try {
				while(rs1.next()) {
					hive_table1 = rs1.getString(1);
					hiveUtil.changeDatabase(user_name);
					list3 = hiveUtil.getTableData(hive_table1);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				list3 = null;
			}
			out.print(JSONObject.fromObject(JsonBackData.successRtnObject(list3)));
			dbUtil.close();
			
			break;
		}
		out.close();
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
