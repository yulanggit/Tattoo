package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.topOne.JsonBackData.JsonBackData;
import com.topOne.bean.Data_source1;
import com.topOne.util.DBUtil;
import com.topOne.util.HDFSUtil;
import com.topOne.util.HiveUtil;
import com.topOne.util.PropertiesUtil;

import net.sf.json.JSONObject;

/**
 * Servlet implementation class ManagerDataServlet
 */
@WebServlet("/ManagerDataServlet")
public class ManagerDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ManagerDataServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String method = request.getParameter("method");
		DBUtil dbUtil = new DBUtil();
		HiveUtil hiveUtil = new HiveUtil();
		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
		String hostName = propertiesUtil.readPropertyByKey("hostName");
		HDFSUtil hdfsUtil = new HDFSUtil(hostName);
		String sql = "";
		String userId ="";
		String userName = "";
		int resultInt = -1;
		switch (method) {
		case "selectAll":
			userId = request.getParameter("userId");
			sql = "select * from data_source where user_id = "+ userId;
			ResultSet rs = dbUtil.executeQuery(sql, null);
			List<Data_source1> list =  new ArrayList<>();
			try {
				while (rs.next()) {
					list.add(new Data_source1(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
							rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getString(9)));
				}
				for (Data_source1 data_source1 : list) {
					System.out.println(data_source1);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				list = null;
			}
			out.print(JSONObject.fromObject(JsonBackData.successRtnObject(list)));
			out.close();
			break;
		case "delete":
			String source_id = request.getParameter("source_id");
			userName = request.getParameter("userName");
			String hdfs_path = "";
			String hive_table = "";
			String source_type = "";
			hiveUtil.changeDatabase(userName);
			// 获取hdfs路径和hive表
			sql="select source_type,hdfs_path,hive_table from data_source where id = ?";
			rs = dbUtil.executeQuery(sql, new Object[] {source_id});
			try {
				while (rs.next()) {
					source_type = rs.getString(1);
					hdfs_path = rs.getString(2);
					hive_table = rs.getString(3);
					// 删除hdfs路径或hive表
					System.out.println(source_type);
					if("hive".equals(source_type)) {
						System.out.println(1111);
						hiveUtil.deletetable(hive_table);
					}else {
						System.out.println(2222);
						hdfsUtil.delete(hdfs_path, true);
					}
					// 删除MySQL数据信息表
					sql = "delete from data_source where id = ?";
					resultInt = dbUtil.executeUpdate(sql, new Object[] {source_id});
					out.print(resultInt);
					out.close();
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case "tableinfo":
			userName = request.getParameter("userName");
			String tableName = request.getParameter("table_name");
			hiveUtil.changeDatabase(userName);
			List<String> list2 = hiveUtil.getTableData3(tableName);
			if(list2!=null) {
				out.print(JSONObject.fromObject(JsonBackData.successRtnObject(list2)));
				out.close();
			}else {
				out.print(JSONObject.fromObject(JsonBackData.fail()));
			}
		default:
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
