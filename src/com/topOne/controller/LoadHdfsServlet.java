package com.topOne.controller;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.hadoop.hive.ql.parse.HiveParser_IdentifiersParser.booleanValue_return;

import com.topOne.util.DBUtil;
import com.topOne.util.HDFSUtil;
import com.topOne.util.PropertiesUtil;



/**
 * Servlet implementation class LoadHdfsServlet
 */
@WebServlet("/LoadHdfsServlet")
@MultipartConfig
public class LoadHdfsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoadHdfsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 设置编码
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html ; charset=UTF-8");
		PrintWriter out = response.getWriter();
		//常用工具类
		DBUtil dbUtil = new DBUtil();
		ResultSet rs = null;
		String hdfspath="";//个人空间
		String hdfscommon="";//公共空间
		
		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
		String hostName = propertiesUtil.readPropertyByKey("hostName");
		HDFSUtil hdfsUtil = new HDFSUtil(hostName);
	
		String id = request.getParameter("id");//用户id
		System.out.println(id);
		String userName=request.getParameter("userName");
		System.out.println(userName);//用户名
		
		
		// 数据源名称
		String sourcename = request.getParameter("sourceName");
		System.out.println(sourcename);
		
		//自定义路径
		String zdy = request.getParameter("zdy");
		System.out.println(zdy);
		
		// 上传空间
		String targetDir = request.getParameter("targetDir");
		System.out.println(targetDir);
		
		// 选择分组
		String groupid = request.getParameter("group_name");
		System.out.println(groupid);//分组id
		
		// 使用Part接收文件
		Part part = request.getPart("data");
		// 自定义路径
		String path = "D:\\";
		// 自定义文件名
		String fileName =request.getParameter("zdifile");

		String filepath = path + File.separator + fileName;
		// 使用write方法向路径中写入文件
		part.write(filepath);
		String sql1="select * from user where id=?";
		rs=dbUtil.executeQuery(sql1, new Object [] {id});
		try {
			while(rs.next()) {
				hdfspath=rs.getString(8);
				hdfscommon=rs.getString(10);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if("ownSpace".equals(targetDir)) {
			boolean outcome;
			hdfspath+=zdy;
			hdfsUtil.mkdirs2(hdfspath);
			hdfsUtil.upLoad(true, true, new String[] { filepath },hdfspath );
			String sql2 = "insert into data_source(source_name,source_type,hdfs_path,user_id,group_id) values(?,?,?,?,?)";
			int result=dbUtil.executeUpdate(sql2,new Object [] {sourcename,"hdfs",hdfspath,id,groupid});
			if(result!=-1) {
				outcome=true;
				response.sendRedirect("/Tattoo/LoadHdfs.jsp?outcome="+outcome);
			}else {
				outcome=false;
				response.sendRedirect("/Tattoo/LoadHdfs.jsp?outcome="+outcome);
			}
		
		}else if("commonSpace".equals(targetDir)){
			boolean outcome;
			hdfscommon+=zdy;
			hdfsUtil.mkdirs2(hdfscommon);
			hdfsUtil.upLoad(true, true, new String[] { filepath },hdfscommon);
			String sql2 = "insert into data_source(source_name,source_type,hdfs_path,user_id,group_id) values(?,?,?,?,?)";
			int result=dbUtil.executeUpdate(sql2,new Object [] {sourcename,"hdfs",hdfscommon,id,groupid});
			
			if(result!=-1) {
				outcome=true;
				response.sendRedirect("/Tattoo/LoadHdfs.jsp?outcome="+outcome);
			}else {
				outcome=false;
				response.sendRedirect("/Tattoo/LoadHdfs.jsp?outcome="+outcome);
			}
		
		
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
