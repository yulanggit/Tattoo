package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.xml.internal.ws.policy.EffectiveAlternativeSelector;
import com.topOne.JsonBackData.JsonBackData;
import com.topOne.bean.User;
import com.topOne.util.DBUtil;

import net.sf.json.JSONObject;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8"); 
		response.setContentType("text/plain ; charset=UTF-8");  
        PrintWriter out = response.getWriter(); 
        String username = request.getParameter("username");  
        String password = request.getParameter("password"); 
        DBUtil dbUtil=new DBUtil();
        ResultSet rs=dbUtil.executeQuery("select id,user_name,user_pwd,name from user where user_name = ? and user_pwd = ?",new Object[] {username,password});
        List<User> list=new ArrayList<>();
        User user=null;
        try {
			while(rs.next()) {
				user=new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
				list.add(user);
				out.println(JSONObject.fromObject(JsonBackData.successRtnObject(list)));
				out.flush();
				
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
        out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
