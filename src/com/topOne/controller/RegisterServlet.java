package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Out;

import com.topOne.service.Create_path;
import com.topOne.util.DBUtil;
import com.topOne.util.HDFSUtil;
import com.topOne.util.HiveUtil;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//response.setContentType("text/plain ; charset=UTF-8"); 
		String method = request.getParameter("method");
		PrintWriter out = response.getWriter();
		
		switch (method) {
		case "register":
			//获取页面信息
			String user_name = request.getParameter("user_name");
			String user_pwd = request.getParameter("user_pwd");
			String name = request.getParameter("name");
			String type = request.getParameter("type");
			String birthday = request.getParameter("birthday");
			System.out.println(user_name + user_pwd + type + name +  birthday);
			//只要有一项没有输入，返回失败信息
			boolean bool = (user_name != null)&&(user_pwd != null)&&(name != null)&&(type != null)&&(birthday != null);
			System.out.println(bool);
			DBUtil dbUtil = new DBUtil();
			String sql1 = "select user_name FROM user WHERE user_name = ?";
			ResultSet rs = dbUtil.executeQuery(sql1, new Object[] {user_name});
			try {
				if (bool) {
					
				
				if (!rs.next()) {
					System.out.println(0);
					//登录账号不存在，可以注册,将信息存入到user表  
					//注册成功后开始创建HDFS空间和Hive数据库
					Create_path create_path = new Create_path();
					
					String HDFSpath = create_path.HDFSpath(user_name);
					
					String HivedatabaseName = create_path.HiveBase(user_name);
					
					//(new HiveUtil()).createCommondatabase("common");
					
					Object[] objects = new Object[] {user_name,user_pwd,name,type,birthday,HDFSpath,HivedatabaseName,"/Tattoo/hive/common/","common"};
					String sql2 = "insert into user(user_name,user_pwd,name,type,birthday,HDFS_path,Hive_database,HDFS_common,Hive_common) values(?,?,?,?,?,?,?,?,?)";
					if ( dbUtil.executeUpdate(sql2, objects) != -1) {
						//获取到user_id
						String sql3 = "select id from user where user_name = ?";
						ResultSet rSet = dbUtil.executeQuery(sql3, new Object[] {user_name});
						int user_id = 0;
						while(rSet.next()) {
							 user_id = rSet.getInt(1);
						}
						//向source_group插入数据
						String sql4 = "insert into source_group(group_name,user_id) values(?,?)";
						
						if ( dbUtil.executeUpdate(sql4, new Object[] {"default",user_id}) != -1) {
							out.print(true);
						} else {
							out.print(false);
						}
					}else {
						out.print(false);
					}
				}else {
					//登录账号存在，不可以注册
					out.print(false);
				}
				}else {
					//注册信息不完善，不可以注册
					out.print(false);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			break;
		default:
			break;
		}
		out.close();
	}
	

	
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
