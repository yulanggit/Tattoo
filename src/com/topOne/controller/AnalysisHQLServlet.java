package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.topOne.JsonBackData.JsonBackData;
import com.topOne.bean.Data_flowStatus;
import com.topOne.bean.Flowinforecord;
import com.topOne.service.CountAnalysis;
import com.topOne.util.DBUtil;
import com.topOne.util.HiveUtil;
import com.topOne.util.PropertiesUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class AnalysisHQLServlet
 */
@WebServlet("/AnalysisHQLServlet")
public class AnalysisHQLServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AnalysisHQLServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain ; charset=UTF-8");
		PrintWriter out = response.getWriter();
		DBUtil dbUtil = new DBUtil();
		String method = request.getParameter("method");
		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
		String hostName = propertiesUtil.readPropertyByKey("hostName");
		HiveUtil hiveUtil = new HiveUtil();
		// 获取当前登录用户的user_id、user_name
		String user_id = request.getParameter("user_id");
		System.out.println("user_id=" + user_id);
		//String user_name = (new CountAnalysis()).userName(user_id);
		String user_name = request.getParameter("user_name");
		System.out.println(user_name);
		List<String> test = new ArrayList<>();
		test.add(null);
		Object  id = 0;
		// 接收页面数据
		String resultTable = request.getParameter("resultTable");
		int dataSourceId = Integer.valueOf(request.getParameter("dataSourceId"));
		String ownsql = request.getParameter("ownsql");
		String tableName =resultTable;
		switch (method) {
		case "flushStatus1":
			//第一次插入运行状态
			//flow_name,source_id,hive_sql,flow_status,result_table,user_id,   6
			String firstsql = "insert into data_flow(flow_name,source_id,flow_type,hive_sql,flow_status,result_table,user_id) values(?,?,?,?,?,?,?)";
			Object[] objects = new Object[] {"用户自定义",dataSourceId,"HQL",ownsql,"运行中",tableName,user_id};
			boolean firstbool = dbUtil.executeUpdate(firstsql, objects) != -1;//-1为插入失败
			if (firstbool) {
				//取出状态信息，并返回页面
				String sql = "select * from data_flow where user_id = ? order by id desc limit 1";
				ResultSet rs = dbUtil.executeQuery(sql, new Object[] {user_id});
				System.out.println(sql);
				List<Data_flowStatus> list = new ArrayList<>();
				try {
					while (rs.next()) {
						list.add(new Data_flowStatus(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4),
								rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),rs.getString(9),
								rs.getInt(10), rs.getString(11)));
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String result = JSONArray.fromObject(list).toString();
				System.out.println("1"+result);
				out.print(result);
				out.close();
			} 
			
			break;
		case "HQLcalculation":
			// ownsql、common只能执行其中的一个（优先自动选择ownsql）
			String common = null;
			if (ownsql == null) {
				common = request.getParameter("common");
				if (common != null) {
					// 执行common
				}else {
					out.print(JSONObject.fromObject(JsonBackData.successRtnObject(test)));
					out.close();
				}
			}else if (ownsql != null) {
				// 执行ownsql
				hiveUtil.changeDatabase(user_name);
				String sql = "create table " + tableName + " as " + ownsql;
				boolean funResult = hiveUtil.execute(sql);
				System.out.println("funResult=" + funResult);
				//取出对应的id
				String sqlsecond = "select id from data_flow where user_id = ? order by id desc limit 1";
				ResultSet rSet1 = dbUtil.executeQuery(sqlsecond, new Object[] {user_id});
				try {
					while(rSet1.next()) {
						id = rSet1.getInt(1);
						System.out.println("id=" + id);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				if (funResult) {
					//把结果表显示在页面，并将结果表信息记录到data_flow
					List<String> list = new ArrayList<>();
					list = hiveUtil.getTableDataAll(tableName);
					System.out.println("计算结果" + list);
					out.print(JSONObject.fromObject(JsonBackData.successRtnObject(list)));
					out.close();
					String sqlInsert = "update data_flow set flow_status = '已完成' where id = ?";
					//可在此添加是否保存计算记录
					int inResult = dbUtil.executeUpdate(sqlInsert, new Object[] {id});
					if (inResult != -1) {
						System.out.println("计算记录插入成功");
					}
				}else {
					//返回错误信息，计算失败,请检查sql语句
					String sqlInsert = "update data_flow set flow_status = '异常' where id = ?";
					dbUtil.executeUpdate(sqlInsert, new Object[] {id});
					out.print(JSONObject.fromObject(JsonBackData.successRtnObject(test)));
					out.close();
				}
			}
			System.out.println(tableName + "\t" + dataSourceId + "\t" + common);
			break;
			
		case "flushStatus2":
			//优化时，可单独封装成方法
			String sql = "select * from data_flow where user_id = ? order by id desc limit 1";
			ResultSet rs = dbUtil.executeQuery(sql, new Object[] {user_id});
			System.out.println(sql);
			List<Data_flowStatus> list = new ArrayList<>();
			try {
				while (rs.next()) {
					list.add(new Data_flowStatus(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4),
							rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),rs.getString(9),
							rs.getInt(10), rs.getString(11)));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String result = JSONArray.fromObject(list).toString();
			System.out.println("3"+result);
			out.print(result);
			out.close();
			break;
		}
		out.close();
		dbUtil.close();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
