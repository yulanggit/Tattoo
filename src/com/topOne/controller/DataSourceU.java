package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.hadoop.hive.metastore.api.ThriftHiveMetastore.create_database_args;

import com.sun.swing.internal.plaf.basic.resources.basic;
import com.topOne.bean.Source_group;
import com.topOne.bean.Source_groupUser;
import com.topOne.service.Data_sourceSelect;
import com.topOne.util.DBUtil;

import net.sf.json.JSONArray;

/**
 * Servlet implementation class DataSourceU
 */
@WebServlet("/DataSourceU")
public class DataSourceU extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataSourceU() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		String method = request.getParameter("method");
		Data_sourceSelect data_sourceSelect = new Data_sourceSelect();
		switch (method) {
		//删除数据源
		case "delete":
			String id = request.getParameter("id");
		
			Boolean result = data_sourceSelect.deleteDatasource(Integer.parseInt(id));
			if(result==true) {
				out.print("success");
				out.close();
			}else {
				out.print("error");
				out.close();
			}
			break;
			//查询所有数据源
		case "selectAllSource_group":
			
			List<Source_groupUser> list1 = data_sourceSelect.selectAllSource_group();
			//out.print(JSONArray.fromObject(list1));
			//out.close();
			request.setAttribute("list1", list1);
			request.getRequestDispatcher("source_group.jsp").forward(request, response);
	        break;
			//删除分组
		case "deleteSource_group":
			String id1 = request.getParameter("id");
			
			Boolean result2 = data_sourceSelect.deleteSource_group(Integer.parseInt(id1));
			if(result2==true) {
				out.print("success");
				out.close();
			}else {
				out.print("error");
				out.close();
			}
			break;
			//校验添加分组
		case "checkGroup":
			String add_groupName = request.getParameter("add_groupName");
			
			Boolean resultC = data_sourceSelect.checkGroup(add_groupName);
			if(resultC==false) {
				out.print("success");
			}else {
				out.print("error");
			}
			out.close();
			break;
		case "addG":
			String add_groupName1 = request.getParameter("add_groupName");
			Boolean resultG = data_sourceSelect.addG(add_groupName1);
			if (resultG==true) {
				out.print("success");
			}else {
				out.print("error");
			}
			break;
		default:
			break;
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
