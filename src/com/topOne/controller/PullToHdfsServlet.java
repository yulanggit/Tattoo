package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.topOne.JsonBackData.JsonBackData;
import com.topOne.bean.DatabaseInfo;
import com.topOne.util.DBUtil;
import com.topOne.util.HDFSUtil;
import com.topOne.util.HiveUtil;
import com.topOne.util.PropertiesUtil;
import com.topOne.util.RemoteUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class PullToServlet
 */
@WebServlet("/PullToHdfsServlet")
public class PullToHdfsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PullToHdfsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html ; charset=UTF-8");
		HiveUtil hiveUtil=new HiveUtil();
		
		
		
		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
		String host = propertiesUtil.readPropertyByKey("hostName");
		String userName = propertiesUtil.readPropertyByKey("hadoopUser");
		String userPwd = propertiesUtil.readPropertyByKey("hadoopPwd");
		HDFSUtil hdfsUtil=new HDFSUtil(host);
		RemoteUtil remoteUtil = new RemoteUtil(host, userName, userPwd);
		String cmd = "source .bash_profile && ";	
        PrintWriter out = response.getWriter(); 
		String method=request.getParameter("method");
		DBUtil dbUtil=new DBUtil();
		List<DatabaseInfo>  list=new ArrayList<>();
		int database;
		String databasename="";
		String hostName="";
		String port="";
		String name="";
		String password="";
		ResultSet rs=null;
		String sql="";
		String jdbcurl="";
		String hdfspath="";//个人空间
		String hdfscommon="";//公共空间
		String zdy="";//自定义路径
		switch (method) {
		case "SingleImport" :
			//指定空间
			String targetDir = request.getParameter("space");
			System.out.println(targetDir);
			
			String group_id=request.getParameter("group_id");
			System.out.println(group_id);
			String group_name=request.getParameter("group_name");
			System.out.println(group_name);
			String sourcename=request.getParameter("sourcename");
			System.out.println(sourcename);
			
			String table=request.getParameter("table");
			System.out.println(table);
			String id=request.getParameter("id");
			System.out.println(id);
			
			database=Integer.parseInt(request.getParameter("database"));
			zdy=request.getParameter("zdy");
			System.out.println(zdy);
			String sql1="select * from user where id=?";
			
			rs=dbUtil.executeQuery(sql1, new Object [] {id});
			try {
				while(rs.next()) {
					hdfspath=rs.getString(8);
					hdfscommon=rs.getString(10);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			sql="select databasename,hostName,port,name,password from dbconnectinfo where id="+database;
			rs=dbUtil.executeQuery(sql, null);
			try {
				while(rs.next()) {
					databasename=rs.getString(1);
					hostName=rs.getString(2);
					port=rs.getString(3);
					name=rs.getString(4);
					password=rs.getString(5);
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			jdbcurl="jdbc:mysql"+ "://" + hostName + ":" + port + "/"+databasename;
			if("ownSpace".equals(targetDir)) {
				hdfspath+=zdy;
				cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --table "+table+" --target-dir "+hdfspath+" --fields-terminated-by '\\t' "+" -m 1";
				System.out.println(cmd);
				String status=remoteUtil.execute(cmd);
				if(!status.contains("Exception")) {
					
					sql="insert into data_source(source_name,source_type,hdfs_path,user_id,group_id) values(?,'hdfs',?,?,?)";
					int result=dbUtil.executeUpdate(sql,new Object [] {sourcename,hdfspath,id,group_id});
					
					out.print(JSONObject.fromObject(JsonBackData.success()));
					if(result!=-1) {
						System.out.println("插入成功");
					}else {
						System.out.println("插入失败");
					}
				
				}
			
				else {
					out.print(JSONObject.fromObject(JsonBackData.fail()));
				}
			
			}
			else if("commonSpace".equals(targetDir)){
				hdfscommon+=zdy;
				cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --table "+table+" --target-dir "+hdfspath+" --fields-terminated-by '\\t' "+" -m 1";
				System.out.println(cmd);
				String status=remoteUtil.execute(cmd);
				if(!status.contains("Exception")) {
					sql="insert into data_source(source_name,source_type,hdfs_path,user_id,group_id) values(?,'hdfs',?,?,?)";
					int result=dbUtil.executeUpdate(sql,new Object [] {sourcename,hdfscommon,id,group_id});
					out.print(JSONObject.fromObject(JsonBackData.success()));
					if(result!=-1) {
						System.out.println("插入成功");
					}else {
						System.out.println("插入失败");
					}
				}
				else {
					out.print(JSONObject.fromObject(JsonBackData.fail()));
				}
			}
			break;
		case "SingleImport2" :
			boolean overwrite=Boolean.parseBoolean(request.getParameter("overwrite"));
			table=request.getParameter("table");
			database=Integer.parseInt(request.getParameter("database"));
			hdfspath=request.getParameter("hdfs_path");
			sql="select databasename,hostName,port,name,password from dbconnectinfo where id="+database;
			rs=dbUtil.executeQuery(sql, null);
			try {
				while(rs.next()) {
					databasename=rs.getString(1);
					hostName=rs.getString(2);
					port=rs.getString(3);
					name=rs.getString(4);
					password=rs.getString(5);
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			jdbcurl="jdbc:mysql"+ "://" + hostName + ":" + port + "/"+databasename;
			
			if(!overwrite) {
				//追加导入
				cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --table "+table+" --append "+" --target-dir "+hdfspath+" --fields-terminated-by '\\t' "+" -m 1";
			}else {
				//覆盖导入
				cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --table "+table+" --delete-target-dir "+hdfspath+" --fields-terminated-by '\\t' "+" -m 1";
			}
			
			System.out.println(cmd);
			String status=remoteUtil.execute(cmd);
			if(!status.contains("Exception")) {
				out.print(JSONObject.fromObject(JsonBackData.success()));
			}else {
				
				out.print(JSONObject.fromObject(JsonBackData.fail()));
			}
			break;
		case "sqlImport" :
			//指定空间
			targetDir = request.getParameter("space");
			System.out.println(targetDir);
			
			group_id=request.getParameter("group_id");
			group_name=request.getParameter("group_name");
			sourcename=request.getParameter("sourcename");
			table=request.getParameter("table");
			id=request.getParameter("id");
		    String sqlcustom=request.getParameter("sqlcustom");
		    zdy=request.getParameter("zdy");
		    
		    
		   
		    database=Integer.parseInt(request.getParameter("database"));
			
		    sql1="select * from user where id=?";
			
			rs=dbUtil.executeQuery(sql1, new Object [] {id});
			try {
				while(rs.next()) {
					hdfspath=rs.getString(8);
					hdfscommon=rs.getString(10);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		    sql="select databasename,hostName,port,name,password from dbconnectinfo where id="+database;
			rs=dbUtil.executeQuery(sql, null);
			try {
				while(rs.next()) {
					databasename=rs.getString(1);
					hostName=rs.getString(2);
					port=rs.getString(3);
					name=rs.getString(4);
					password=rs.getString(5);
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			jdbcurl="jdbc:mysql"+ "://" + hostName + ":" + port + "/"+databasename;
			
			if("ownSpace".equals(targetDir)) {
				hdfspath+=zdy;
				cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --query "+"'"+sqlcustom+" where 1=1 and $CONDITIONS"+"'"+" --target-dir "+hdfspath+" --fields-terminated-by '\\t' "+" -m 1";
				System.out.println(cmd);
				status=remoteUtil.execute(cmd);
				if(!status.contains("Exception")) {
					sql="insert into data_source(source_name,source_type,hdfs_path,user_id,group_id) values(?,'hdfs',?,?,?)";
					int result=dbUtil.executeUpdate(sql,new Object [] {sourcename,hdfspath,id,group_id});
					out.print(JSONObject.fromObject(JsonBackData.success()));
					if(result!=-1) {
						System.out.println("插入成功");
					}else{
						System.out.println("插入失败");
					}
				}
				else {
					out.print(JSONObject.fromObject(JsonBackData.fail()));
				}
			}
			else if("commonSpace".equals(targetDir)){
				hdfscommon+=zdy;
				cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --table "+table+" --target-dir "+hdfscommon+" --fields-terminated-by '\\t' "+" -m 1";
				System.out.println(cmd);
				status=remoteUtil.execute(cmd);
				if(!status.contains("Exception")) {
					sql="insert into data_source(source_name,source_type,hdfs_path,user_id,group_id) values(?,'hdfs',?,?,?)";
					int result=dbUtil.executeUpdate(sql,new Object [] {sourcename,hdfscommon,id,group_id});
					out.print(JSONObject.fromObject(JsonBackData.success()));
					if(result!=-1) {
						System.out.println("插入成功");
					}else {
						System.out.println("插入失败");
					}
				}
				else {
					out.print(JSONObject.fromObject(JsonBackData.fail()));
				}
			}
			break;
			
		case "sqlImport2" :
			
			overwrite=Boolean.parseBoolean(request.getParameter("overwrite"));
			sqlcustom=request.getParameter("sqlcustom");
			database=Integer.parseInt(request.getParameter("database"));
			hdfspath=request.getParameter("hdfs_path");
			sql="select databasename,hostName,port,name,password from dbconnectinfo where id="+database;
			rs=dbUtil.executeQuery(sql, null);
			try {
				while(rs.next()) {
					databasename=rs.getString(1);
					hostName=rs.getString(2);
					port=rs.getString(3);
					name=rs.getString(4);
					password=rs.getString(5);
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			jdbcurl="jdbc:mysql"+ "://" + hostName + ":" + port + "/"+databasename;
			if(!overwrite) {
				//追加导入
				cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --query "+"'"+sqlcustom+" where 1=1 and $CONDITIONS"+"'"+" --append "+" --target-dir "+hdfspath+" --fields-terminated-by '\\t' "+" -m 1";
			}else {
				//覆盖导入
				cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --query "+"'"+sqlcustom+" where 1=1 and $CONDITIONS"+"'"+" --delete-target-dir "+hdfspath+" --fields-terminated-by '\\t' "+" -m 1";
			}
			
			System.out.println(cmd);
			status=remoteUtil.execute(cmd);
			if(!status.contains("Exception")) {
				
				out.print(JSONObject.fromObject(JsonBackData.success()));
			}
			else {
				out.print(JSONObject.fromObject(JsonBackData.fail()));
			}
			break;
		default:
			break;
		
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
