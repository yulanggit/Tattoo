package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.topOne.util.HiveUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class ShowServlet
 */
@WebServlet("/ShowServlet")
public class ShowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String type = request.getParameter("type");
		String databaseName = request.getParameter("HdatabaseName");
		String tableName = request.getParameter("tableName");
		String name2 = request.getParameter("name2");
		String value = request.getParameter("value");
		String xAxis = request.getParameter("xAxis");
		//String yAxis = request.getParameter("yAxis");
		//获取js页面传过来的数组
		String[] yAxis = request.getParameterValues("yAxis");
		
		String method = request.getParameter("method");
		HiveUtil hiveUtil = new HiveUtil();
		hiveUtil.changeDatabase(databaseName);
		String sql = "";
		switch (method) {
		
		case "showgetData":
			hiveUtil.changeDatabase(databaseName);
			/*System.out.println(databaseName);
			System.out.println(tableName);*/
			List<String> tableInfo = hiveUtil.getTableInfo(tableName);
			
			List<String> xandyList = new ArrayList<>();
		
		
			if ("line".equals(type) || "bar".equals(type)) {
				
				for (String string : tableInfo) {
					xandyList.add(string.split("\t")[0]);
					
				}
				out.print(JSONArray.fromObject(xandyList).toString());
				out.close();
			}else if ("pie1".equals(type)) {
				List<String> nandvList = new ArrayList<>();
				for (String string : tableInfo) {
					nandvList.add(string.split("\t")[0]);
					
				}
				out.print(JSONArray.fromObject(nandvList).toString());
				out.close();
				
			}
			break;	
		case "showChart":
			hiveUtil.changeDatabase(databaseName);
					if ("line".equals(type) || "bar".equals(type)) {
						
						Map<String, List<String>> map = new HashMap<>();
						 String kk = "";
						 //根据顺序拼接相应的字符串
					     String endstr = xAxis + ",";
						 for (int i = 0; i < yAxis.length; i++)  //根据数组的元素个数判断循环次数
					        {
					            kk = yAxis[i] ;  //在每个元素前后加上我们想要的格式，效果例如：
					            if (i < yAxis.length - 1)  //根据数组元素的个数来判断应该加多少个逗号
					            {
					                kk += ",";
					            }
					            endstr += kk;
					        }
					       sql = "select " + endstr + " from "+tableName;
					       List<String> result = hiveUtil.getSomeColumnData(sql, endstr);
					       
					       map.put("xData", new ArrayList<>());
					       for(int i = 1;i <= yAxis.length;i ++) {
					    	   map.put("yData" + i, new ArrayList<>());
					       }
					       //根据拼接字符串顺序查询的数据也对应该字符串数据，将数据根据顺序相应切割
					       for (String line : result) {
					    	   map.get("xData").add(line.split("\t")[0]);
							for (int i = 1;i <= yAxis.length;i ++) {
								map.get("yData" + i).add(line.split("\t")[i]);
							}
						}
					       
						
						// 发送数据
						out.print(JSONObject.fromObject(map).toString());
						out.close();
					}else if ("pie1".equals(type)) {
						
						List<String> innerList = hiveUtil.getTableData(tableName);
						 Map<String, List> map2 = new HashMap<>();
						 List<String> legendData = new ArrayList<>();
							List<String> innerData = new ArrayList<>();
							for (String line : innerList) {
								//获取表结构
								List<String> info1 = hiveUtil.getTableInfo(tableName);
								List<String> info = new ArrayList<>();
								for (String string : info1) {
									info.add(string.split("\t")[0]);
								}
								for (int i=0;i<info.size();i++) {
								
									if((name2).equals(info.get(i))) {
										innerData.add(line.split("\t")[i]);
									}
									if((value).equals(info.get(i))) {
										legendData.add(line.split("\t")[i]);
									}
								}
							}
							
							map2.put("innerData", innerData);
							map2.put("legendData", legendData);
							out.print(JSONObject.fromObject(map2).toString());
							out.close();
							
				
				 break;
			}
		default:
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
