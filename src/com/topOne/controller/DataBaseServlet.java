package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.topOne.util.DBUtil;
import com.topOne.util.HiveUtil;
import com.topOne.util.PropertiesUtil;
import com.topOne.util.RemoteUtil;


import net.sf.json.JSONArray;

/**
 * Servlet implementation class DataBaseServlet
 */
@WebServlet("/DataBaseServlet")
public class DataBaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataBaseServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String method = request.getParameter("method");
		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
		String host = propertiesUtil.readPropertyByKey("hostName");
		String userName = propertiesUtil.readPropertyByKey("hadoopUser");
		String userPwd = propertiesUtil.readPropertyByKey("hadoopPwd");
		// 需要连接的数据库的相关参数
		HiveUtil hiveUtil = new HiveUtil();
		//String databaseType = request.getParameter("databaseType");
		String hostName = request.getParameter("hostName");
		String port = request.getParameter("port");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String databasename=request.getParameter("databasename");
		String jdbcUrl = "jdbc:mysql"+ "://" + hostName + ":" + port + "/"+databasename;
		RemoteUtil remoteUtil = new RemoteUtil(host, userName, userPwd);
		String cmd = "source .bash_profile && ";
		switch (method) {
		case "showDatabases":
			cmd += "sqoop list-databases --connect " + jdbcUrl + " --username " + name + " --password " + password;
			String databaseList = remoteUtil.execute(cmd);
			String databaseArray = JSONArray.fromObject(databaseList.split("\n")).toString();
			out.print(databaseArray);
			out.close();
			break;
		case "showTables":
			String databaseName = request.getParameter("databaseName");
			jdbcUrl += databaseName;
			cmd += "sqoop list-tables --connect " + jdbcUrl + " --username " + name + " --password " + password;
			String tableList = remoteUtil.execute(cmd);
			System.out.println(tableList);
			if (!tableList.contains("Running Sqoop version: 1.4.7")) {
				String tableArray = JSONArray.fromObject(tableList.split("\n")).toString();
				out.print(tableArray);
				out.close();
			}else {
				out.print(JSONArray.fromObject(new ArrayList<String>()));
				out.close();
			}
		case "showHivedatabase":
			String HdatabaseN = request.getParameter("HdatabaseN");
			String resultS = hiveUtil.changeDatabase(HdatabaseN);
			out.print(resultS);
			out.close();
			break;
		case "showtableList":
			String Hivedatabase = request.getParameter("Hivedatabase");
			//获取用户id
			String id = request.getParameter("id");
			System.out.println(id);
			hiveUtil.changeDatabase(Hivedatabase);
			//原先在hive库中未被筛选的表
			//List<String> listY = hiveUtil.getTaleList();
			
			//筛选完成状态的表
			List<String> list = new ArrayList<>();
			DBUtil dbUtil = new DBUtil();
			String sql = "SELECT result_table FROM data_flow WHERE user_id='"+id+"' AND flow_status='已完成'";
			ResultSet rSet = dbUtil.executeQuery(sql, null);
			try {
				while(rSet.next()) {
					list.add(rSet.getString(1));
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
				list=null;
			}
			
			
			String result = JSONArray.fromObject(list).toString();
			out.print(result);
			out.close();
			
			break;
		case "tableInfo":
			String HdatabaseName = request.getParameter("HdatabaseName");
			String tableName = request.getParameter("tableName");
			System.out.println("1"+tableName);
			// 当需要获取表信息时，如果只传入表名称会在当前数据库中搜索该表，需要先切换数据库
			// 如果传入库名.表名 -> 在指定的数据库下进行搜索
			hiveUtil.changeDatabase(HdatabaseName);
			List<String> list1 = hiveUtil.getTableInfo(tableName);
			System.out.println("2"+tableName);
			out.print(JSONArray.fromObject(list1).toString());
			out.close();
			
			break;
		case "tableData":
			String HdatabaseName1 = request.getParameter("HdatabaseName");
			String tableName1 = request.getParameter("tableName");
			
			hiveUtil.changeDatabase(HdatabaseName1);
			// 当需要获取表信息时，如果只传入表名称会在当前数据库中搜索该表，需要先切换数据库
			// 如果传入库名.表名 -> 在指定的数据库下进行搜索
			List<String> list2 = hiveUtil.getTableData(tableName1);
			out.print(JSONArray.fromObject(list2).toString());
			out.close();
			break;
			
		default:
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
