package com.topOne.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.topOne.JsonBackData.JsonBackData;
import com.topOne.util.DBUtil;
import com.topOne.util.HDFSUtil;
import com.topOne.util.HiveUtil;
import com.topOne.util.PropertiesUtil;

import net.sf.json.JSONObject;

/**
 * Servlet implementation class HiveDataManagerServlet
 */
@WebServlet("/HiveDataManagerServlet")
@MultipartConfig
public class HiveDataManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HiveDataManagerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html ; charset=UTF-8");
		String method = request.getParameter("method");
		DBUtil dbUtil = new DBUtil();
		HiveUtil hiveUtil = new HiveUtil();
		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
		String hostName = propertiesUtil.readPropertyByKey("hostName");
		HDFSUtil hdfsUtil = new HDFSUtil(hostName);
		PrintWriter out = response.getWriter();
		String source_id = "";
		String hive_table = "";
		String hdfs_path = "";
		String sql = "";
		String tableName = "";
		String path = "D:\\";
		boolean isOverwrite = false;
		ResultSet rs = null;
		switch (method) {
		case "first":
			source_id = request.getParameter("source_id");
			sql = "select source_type,hive_table,hdfs_path from data_source where id = " + source_id;
			System.out.println(sql);
			String source_type = "";
			rs = dbUtil.executeQuery(sql, null);
			List<String> list = new ArrayList<>();
			try {
				while (rs.next()) {
					source_type = rs.getString(1);
					hive_table = rs.getString(2);
					hdfs_path = rs.getString(3);
					list.add(source_type);
					list.add(hive_table);
					list.add(hdfs_path);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(source_type);
			if (!("".equals(source_type))) {
				out.print(JSONObject.fromObject(JsonBackData.successRtnObject(list)));
			} else {
				out.print(JSONObject.fromObject(JsonBackData.fail()));
			}
			out.close();
			break;

		case "hivefile":
			tableName = request.getParameter("tableName");
			String userName = request.getParameter("userName");
			isOverwrite = request.getParameter("overwrite") == null ? false : true;
			Part part = request.getPart("data");
			// 取出文件名(如果需要)
			// 可以使用自定义的名称，也可以使用UUID
			String fileName = UUID.randomUUID().toString();
			String filepath = path + File.separator + fileName;
			part.write(filepath);
			hdfsUtil.upLoad(true, true, new String[] { filepath }, "/Common");
			String loadToHive = "load data inpath '/Common/" + fileName + "' " + (isOverwrite ? "overwrite" : "")
					+ " into table " + userName + "." + tableName;
			System.out.println(loadToHive);
			Boolean resultBl = hiveUtil.execute(loadToHive);
			response.sendRedirect("/Tattoo/ManagerData1.jsp?resultBl="+resultBl);
			break;
		case "hdfsfile":
			hdfs_path = request.getParameter("hdfs_path");
			System.out.println(hdfs_path);
			isOverwrite = request.getParameter("overwrite") == null ? false : true;
			part = request.getPart("data");
			// 可以使用自定义的名称，也可以使用UUID
			fileName = UUID.randomUUID().toString();
			filepath = path + File.separator + fileName;
			part.write(filepath);
			try {
				if(!isOverwrite) {
					hdfsUtil.upLoad(true, true, new String[] { filepath }, hdfs_path);
				}else {
					hdfsUtil.delete(hdfs_path, true);
					hdfsUtil.mkdirs2(hdfs_path);
					hdfsUtil.upLoad(true, true, new String[] { filepath }, hdfs_path);
				}
				resultBl = true;
			} catch (Exception e) {
				// TODO: handle exception
				resultBl = false;
			}
			response.sendRedirect("/Tattoo/ManagerData1.jsp?resultBl="+resultBl);
			break;
		default:
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
