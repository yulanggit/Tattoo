package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.topOne.util.DBUtil;
import com.topOne.util.HiveUtil;

import net.sf.json.JSONArray;

/**
 * Servlet implementation class execsqlServlet
 */
@WebServlet("/execsqlServlet")
public class execsqlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public execsqlServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String sql = request.getParameter("sql");
		String resultname = request.getParameter("resultname");
		String user_name=request.getParameter("user_name");
		//String user = "admin";
		
		String result_path = "/user/hive/warehouse/test.db/" + resultname;
		System.out.println(result_path);

		// 记录到mysql中
		DBUtil dbUtil = new DBUtil();
		String insertsql = "insert into dataflowmanager(dataflowmanager.`user`,dataflowmanager.`database`,result_path,dataflowmanager.`sql`)  values('"+ user_name +"','"+ user_name +"','"+ result_path +"','"+ sql +"')";
		System.out.println(insertsql);
		
		int resultInt = dbUtil.executeUpdate(insertsql, null);
		if (resultInt == -1) {
			insertsql = "update dataflowmanager set status = '失败' where result_path = ?";
			dbUtil.executeUpdate(insertsql, new Object[] { result_path });
		}
		
		HiveUtil hiveUtil = new HiveUtil();
		hiveUtil.changeDatabase("test");
		List<String> list = hiveUtil.getResultData(sql, resultname);
		// for (String line : list) {
		// System.out.println(line);
		// }
		if (list != null) {
			insertsql = "update dataflowmanager set status = '已完成' where result_path = ?";
			dbUtil.executeUpdate(insertsql, new Object[] { result_path });
			String result = JSONArray.fromObject(list).toString();
			out.print(result);
		}else {
			insertsql = "update dataflowmanager set status = '失败' where result_path = ?";
			dbUtil.executeUpdate(insertsql, new Object[] { result_path });
			String result = JSONArray.fromObject(list).toString();
			out.print(result);
		}
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
