package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.topOne.util.DBUtil;

import net.sf.json.JSONArray;

/**
 * Servlet implementation class DataGroupServlet
 */
@WebServlet("/DatagroupServlet")
@MultipartConfig
public class DataGroupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataGroupServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置编码
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		DBUtil dbUtil=new DBUtil();
		PrintWriter out=response.getWriter();
		String username=request.getParameter("username");
		System.out.println(username);
		
		ResultSet rs=dbUtil.executeQuery("select s.group_name from (select id from user where user_name =?) as result,source_group as s where result.id=s.user_id",new Object[] {username});
		List<String> list=new ArrayList<>();
		try {
			while(rs.next()) {
				list.add(rs.getString(1));
			}
			System.out.println(list);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		out.println(JSONArray.fromObject(list).toString());
		out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
