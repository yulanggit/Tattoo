package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.hadoop.hive.metastore.api.ThriftHiveMetastore.create_database_args;

import com.topOne.JsonBackData.JsonBackData;
import com.topOne.bean.DatabaseInfo;
import com.topOne.util.DBUtil;
import com.topOne.util.HiveUtil;
import com.topOne.util.PropertiesUtil;
import com.topOne.util.RemoteUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


/**
 * Servlet implementation class PullToHiveServlet
 */
@WebServlet("/PullToHiveServlet")
public class PullToHiveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PullToHiveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html ; charset=UTF-8");
		HiveUtil hiveUtil=new HiveUtil();
		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
		String host = propertiesUtil.readPropertyByKey("hostName");
		String userName = propertiesUtil.readPropertyByKey("hadoopUser");
		String userPwd = propertiesUtil.readPropertyByKey("hadoopPwd");
		RemoteUtil remoteUtil = new RemoteUtil(host, userName, userPwd);
		String cmd = "source .bash_profile && ";	
        PrintWriter out = response.getWriter(); 
		String method=request.getParameter("method");
		DBUtil dbUtil=new DBUtil();
		List<DatabaseInfo>  list=new ArrayList<>();
		int database;
		String databasename="";
		String hostName="";
		String port="";
		String name="";
		String password="";
		ResultSet rs=null;
		String sql="";
		String jdbcurl="";
		switch (method) {
		case "obainDB":
			sql="select id,sourcename from dbconnectinfo";
			rs=dbUtil.executeQuery(sql, null);
			try {
				while(rs.next()) {
					DatabaseInfo databaseInfo=new DatabaseInfo(rs.getInt(1),rs.getString(2));
					list.add(databaseInfo);
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			out.println(JSONArray.fromObject(list).toString());
			out.flush();
			out.close();
			break;
		case "getdatabase":
			
			database=Integer.parseInt(request.getParameter("database"));
			sql="select databasename,hostName,port,name,password from dbconnectinfo where id="+database;
			rs=dbUtil.executeQuery(sql, null);
			try {
				while(rs.next()) {
					databasename=rs.getString(1);
					hostName=rs.getString(2);
					port=rs.getString(3);
					name=rs.getString(4);
					password=rs.getString(5);
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			jdbcurl="jdbc:mysql"+ "://" + hostName + ":" + port + "/"+databasename;
			cmd += "sqoop list-tables --connect " + jdbcurl + " --username " + name + " --password " + password;
			System.out.println(cmd);
			String tableList = remoteUtil.execute(cmd);
			System.out.println(tableList);
			if (!tableList.contains("Running Sqoop version: 1.4.7")) {
				String tableArray = JSONArray.fromObject(tableList.split("\n")).toString();
				out.print(tableArray);
				out.close();
			}else {
				out.print(JSONArray.fromObject(new ArrayList<String>()));
				out.close();
			}
			break;
		case "SingleImport" :
			String group_id=request.getParameter("group_id");
			String group_name=request.getParameter("group_name");
			String sourcename=request.getParameter("sourcename");
			String hive_dabase=request.getParameter("userName");
			boolean overwrite=Boolean.parseBoolean(request.getParameter("overwrite"));
			String table=request.getParameter("table");
			String id=request.getParameter("id");
			database=Integer.parseInt(request.getParameter("database"));
			String tablename=request.getParameter("tablename");
			sql="select databasename,hostName,port,name,password from dbconnectinfo where id="+database;
			rs=dbUtil.executeQuery(sql, null);
			try {
				while(rs.next()) {
					databasename=rs.getString(1);
					hostName=rs.getString(2);
					port=rs.getString(3);
					name=rs.getString(4);
					password=rs.getString(5);
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			jdbcurl="jdbc:mysql"+ "://" + hostName + ":" + port + "/"+databasename;
			cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --hive-import "+"--hive-database "+hive_dabase+" --table "+table+" --hive-table "+tablename+" -m 1";
			System.out.println(cmd);
			String status=remoteUtil.execute(cmd);
			hiveUtil.changeDatabase(hive_dabase);
			System.out.println(tablename);
			List<String> columNames = hiveUtil.getTableInfo(tablename);
			String columName = "";
			for (String string : columNames) {
				columName += string + ",";
			}
			columName.substring(0, columName.length() - 1);
			System.out.println(columName);
			System.out.println(status);
			if(!status.contains("Exception")) {
				sql="insert into data_source(source_name,source_type,hive_table,table_column,user_id,group_id) values(?,'hive',?,?,?,?)";
				dbUtil.executeUpdate(sql, new Object [] {sourcename,table,columName,id,group_id});
				System.out.println("导入成功");
				out.print(JSONObject.fromObject(JsonBackData.success()));
			}else {
				
				out.print(JSONObject.fromObject(JsonBackData.fail()));
			}
			break;
		case "SingleImport2" :
			System.out.println(123);
			hive_dabase=request.getParameter("userName");
			overwrite=Boolean.parseBoolean(request.getParameter("overwrite"));
			table=request.getParameter("table");
			database=Integer.parseInt(request.getParameter("database"));
			table=request.getParameter("table");
			String tableName=request.getParameter("tableName");
			
			sql="select databasename,hostName,port,name,password from dbconnectinfo where id="+database;
			rs=dbUtil.executeQuery(sql, null);
			try {
				while(rs.next()) {
					databasename=rs.getString(1);
					hostName=rs.getString(2);
					port=rs.getString(3);
					name=rs.getString(4);
					password=rs.getString(5);
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			jdbcurl="jdbc:mysql"+ "://" + hostName + ":" + port + "/"+databasename;
			cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --hive-import "+"--hive-database "+hive_dabase+" --table "+table+" --hive-table "+tableName+(overwrite ? " --hive-overwrite" : "")+" -m 1";
			System.out.println(cmd);
			status=remoteUtil.execute(cmd);
			
			if(!status.contains("Exception")) {
				
				out.print(JSONObject.fromObject(JsonBackData.success()));
			}else {
				
				out.print(JSONObject.fromObject(JsonBackData.fail()));
			}
			break;
		case "sqlImport" :
			group_id=request.getParameter("group_id");
			group_name=request.getParameter("group_name");
			sourcename=request.getParameter("sourcename");
			hive_dabase=request.getParameter("userName");
			
			table=request.getParameter("table");
			id=request.getParameter("id");
			String sqlcustom=request.getParameter("sqlcustom");
			tablename=request.getParameter("tablename");
			
			database=Integer.parseInt(request.getParameter("database"));
			sql="select databasename,hostName,port,name,password from dbconnectinfo where id="+database;
			rs=dbUtil.executeQuery(sql, null);
			try {
				while(rs.next()) {
					databasename=rs.getString(1);
					hostName=rs.getString(2);
					port=rs.getString(3);
					name=rs.getString(4);
					password=rs.getString(5);
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			jdbcurl="jdbc:mysql"+ "://" + hostName + ":" + port + "/"+databasename;
			cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --query "+"'"+sqlcustom+" where 1=1 and $CONDITIONS"+"'"+" --hive-import "+"--hive-database "+hive_dabase+" --hive-table "+tablename+" --target-dir '/tmp/common' "+" -m 1";
			System.out.println(cmd);
			status=remoteUtil.execute(cmd);
			if(!status.contains("Exception")) {
				
				hiveUtil.changeDatabase(hive_dabase);
				columNames = hiveUtil.getTableInfo(table);
				columName = "";
				for (String string : columNames) {
					columName += string + ",";
				}
				columName.substring(0, columName.length() - 1);
				System.out.println(columName);
				
				sql="insert into data_source(source_name,source_type,hive_table,table_column,user_id,group_id) values(?,'hive',?,?,?,?)";
				dbUtil.executeUpdate(sql, new Object [] {sourcename,table,columName,id,group_id});
				System.out.println("导入成功");
				out.print(JSONObject.fromObject(JsonBackData.success()));
			}
			else {
				out.print(JSONObject.fromObject(JsonBackData.fail()));
			}
			break;
		case "sqlImport2" :
			hive_dabase=request.getParameter("userName");
			overwrite=Boolean.parseBoolean(request.getParameter("overwrite"));
			tableName=request.getParameter("tableName");
			// id=request.getParameter("id");
			sqlcustom=request.getParameter("sqlcustom");
			database=Integer.parseInt(request.getParameter("database"));
			sql="select databasename,hostName,port,name,password from dbconnectinfo where id="+database;
			rs=dbUtil.executeQuery(sql, null);
			try {
				while(rs.next()) {
					databasename=rs.getString(1);
					hostName=rs.getString(2);
					port=rs.getString(3);
					name=rs.getString(4);
					password=rs.getString(5);
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			jdbcurl="jdbc:mysql"+ "://" + hostName + ":" + port + "/"+databasename;
			cmd += "sqoop import --connect " + jdbcurl + " --username " + name + " --password " + password+" --query "+"'"+sqlcustom+" where 1=1 and $CONDITIONS"+"'"+" --hive-import "+"--hive-database "+hive_dabase+" --hive-table "+tableName+" --target-dir '/tmp/Common' "+(overwrite ? " --hive-overwrite" : "")+" -m 1";
			System.out.println(cmd);
			status=remoteUtil.execute(cmd);
			if(!status.contains("Exception")) {
				
				out.print(JSONObject.fromObject(JsonBackData.success()));
			}
			else {
				out.print(JSONObject.fromObject(JsonBackData.fail()));
			}
			break;
		default:
			break;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
