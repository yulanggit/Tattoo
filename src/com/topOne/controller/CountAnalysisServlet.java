package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.fabric.xmlrpc.base.Data;
import com.topOne.JsonBackData.JsonBackData;
import com.topOne.bean.Data_flow;
import com.topOne.bean.Data_source;
import com.topOne.bean.Data_source1;
import com.topOne.mr.master.FlowCountMaster;
import com.topOne.mr.master.WordCountMaster;
import com.topOne.service.CountAnalysis;
import com.topOne.util.DBUtil;
import com.topOne.util.HiveUtil;
import com.topOne.util.PropertiesUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class CountAnalysisServlet
 */
@WebServlet("/CountAnalysisServlet")
public class CountAnalysisServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CountAnalysisServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/plain ; charset=UTF-8");
		PrintWriter out = response.getWriter();
		String method = request.getParameter("method");
		String flowname = request.getParameter("flowName");
		PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties"); 
		String hostName =  propertiesUtil.readPropertyByKey("hostName");
		HiveUtil hiveUtil = new HiveUtil();
		//获取当前登录用户的user_id
		String user_id = request.getParameter("user_id");
		System.out.println("user_id=" + user_id);
		String user_name = request.getParameter("user_name");
		//String user_name = (new CountAnalysis()).userName(user_id);
		System.out.println(user_name);
		int source_id ;
		switch (method) {
		case "showDatabasesMR":
			//从数据库(data_source表)中获取数据源
			CountAnalysis cAnalysis = new CountAnalysis();
			List<Data_source1> list = new ArrayList<>();
			list = cAnalysis.mrData("hdfs",user_id);
			System.out.println(list);
			out.print(JSONObject.fromObject(JsonBackData.successRtnObject(list)));
			break;
		case "showDatabasesHQL":
			//从数据库(data_source表)中获取数据源
			CountAnalysis cAnalysis1 = new CountAnalysis();
			List<Data_source> list1 = new ArrayList<>();
			list1 = cAnalysis1.mrData1("hive",user_id);
			System.out.println(list1);
			out.print(JSONObject.fromObject(JsonBackData.successRtnObject(list1)));
			break;
		case "MRcalculation":
			//用户自定义结果表名
			String resultTablename = request.getParameter("resultTablename");
			//用户选择数据源的id
			Integer databaseId = Integer.valueOf(request.getParameter("database"));
			source_id = databaseId;
			//用户指定的算法
			String alg = request.getParameter("alg");
			System.out.println(resultTablename+ "\t" + databaseId+ "\t" +alg);
			
			//处理计算 --> 把计算结果返回MR --js
			switch (alg) {
			case "FlowCount":
				//需要的条件：数据源-
				//将从data_source表中获取的数据源进行流量统计计算，并将结果上传至文件系统
				boolean bool = false;
				CountAnalysis countAnalysis = new CountAnalysis();
				int group_id = countAnalysis.group_id(databaseId);
				String inputPath = countAnalysis.hdfs_path(databaseId);
				inputPath = "hdfs://" + hostName + ":8020" +inputPath;
				System.out.println("inputPath=" + inputPath );
				//resultPath  MR计算后产生的结果路径
				//String resultPath1 = "/Tattoo/hive/" + user_name + resultTablename + new Date().getTime() + "/";
				String resultPath = "hdfs://" + hostName + ":8020" + "/Tattoo/hive/" + user_name + resultTablename + new Date().getTime() + "/part-r-00000";
				
				System.out.println("结果路径="+resultPath);
				
				FlowCountMaster flowCountMaster = new FlowCountMaster();
				
				try {
					//MR统计计算结果
					boolean flowCount = flowCountMaster.FlowCount(inputPath, resultPath);
					System.out.println("MR统计计算结果" + flowCount);
					if (flowCount) {
						bool = true;
						System.out.println("MR统计计算完成");
					}
					System.out.println("flowCount=" + flowCount);
				} catch (Exception e) {
					e.printStackTrace();
					//出现异常则页面返回计算失败，检查文件类型
					bool = false;
					System.out.println("1" + bool);
				}
				//将计算结果转为表，存储在hive数据库中
				System.out.println(user_name);
				hiveUtil.changeDatabase(user_name);
				//String tableName = resultTablename;
				System.out.println(resultTablename);
				if (bool) {
					boolean mkresult = hiveUtil.mkTable(resultTablename, "phoneNum", "string", "upFlow", "int", "downFlow", "int","sumFlow","int");
					System.out.println("mkresult= "+ mkresult);
					if (mkresult) {
						//新建表完成后导入数据并将信息记录在表data_flow
						hiveUtil.changeDatabase(user_name);
						//resultPath += resultPath + "/part-r-00000";
						boolean load =  hiveUtil.load(resultPath, resultTablename);
							if (load) {
								bool = true;
							}else {
								bool = false;
							}
					} else {
						bool = false;
					}
				} 
				System.out.println("5" + bool);
				//将所有信息存储在data_flow表中
				CountAnalysis countAnalysis2 = new CountAnalysis();
				if (bool) {
					bool = countAnalysis2.writeData_flow(new Object[] {flowname,source_id,"MR",alg,"已完成",resultTablename,user_id});
				}
				out.print(bool);
				out.flush();
				out.close();
				//将计算结果表存入data_source  5
				countAnalysis2.writeData_source(new Object[] {resultTablename,"hive",resultTablename,user_id,group_id});
				break;
			case "WordCount":
				boolean bool1 = false;
				CountAnalysis countAnalysis1 = new CountAnalysis();
				String inputPath1 = countAnalysis1.hdfs_path(databaseId);
				inputPath1 = "hdfs://" + hostName + ":8020" +inputPath1;
				System.out.println("inputPath1=" + inputPath1);
				String resultPath1 = "hdfs://" + hostName + ":8020" + "/Tattoo/hive/" + user_name + resultTablename + new Date().getTime() + "/part-r-00000";
				System.out.println("结果路径="+resultPath1);
				WordCountMaster wordCountMaster = new WordCountMaster();
				try {
					//MR统计计算结果
					boolean wordCount = wordCountMaster.WordCount(inputPath1, resultPath1);
					if (wordCount) {
						bool1 = true;
						System.out.println("MR统计计算完成");
					}
				} catch (Exception e) {
					e.printStackTrace();
					bool1 = false;
					System.out.println("1=" + bool1);
				}
				System.out.println("2=" + bool1);
				//出现异常则页面返回计算失败，检查文件类型
				//将计算结果转为表，存储在hive数据库中
				hiveUtil.changeDatabase(user_name);
				//String tableName1 = resultTablename + new Date().getTime();
				System.out.println("resultTablename" + resultTablename);
				if (bool1) { 
					boolean mkresult = hiveUtil.mkTable(resultTablename,"word","string","sum","int");
					System.out.println("mkresult= "+ mkresult);
					if (mkresult) {
						//新建表完成后导入数据并将信息记录在表data_flow
						hiveUtil.changeDatabase(user_name);
						//resultPath += resultPath + "/part-r-00000";
						boolean load =  hiveUtil.load(resultPath1, resultTablename);
							if (load) {
								bool1 = true;
							}else {
								bool1 = false;
							}
					} else {
						bool1 = false;
						System.out.println("4" + bool1);
					}
					//信息导入完成后，将信息存入data_flow表中
					if (bool1) {
						CountAnalysis countAnalysis21 = new CountAnalysis();
						bool1 = countAnalysis21.writeData_flow(new Object[] {flowname,source_id,"MR",alg,"已完成",resultTablename,user_id});
					}
					out.print(bool1);
					out.flush();
					out.close();
					//将统计结果表存入data_source
					int group_id1 = countAnalysis1.group_id(databaseId);
					countAnalysis1.writeData_source(new Object[] {resultTablename,"hive",resultTablename,user_id,group_id1});
				}
			break;
			}
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
