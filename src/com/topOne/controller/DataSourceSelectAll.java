package com.topOne.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.topOne.bean.Data_source;
import com.topOne.service.Data_sourceSelect;
import com.topOne.util.PageUtil;

/**
 * Servlet implementation class DataSourceSelectAll
 */
@WebServlet("/DataSourceSelectAll")
public class DataSourceSelectAll extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataSourceSelectAll() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pageNumber = request.getParameter("pageNumber");	
		if("null".equals(pageNumber)||pageNumber==null) {
			pageNumber="1";
		}
		PageUtil pageUtil = new PageUtil(Integer.parseInt(pageNumber), "data_source");
		Data_sourceSelect data_sourceSelect = new Data_sourceSelect();
		List<Data_source> list = data_sourceSelect.selectByPage(pageUtil);
		//System.out.println(list);
		request.setAttribute("list",list);
		request.setAttribute("page", pageUtil);
		request.getRequestDispatcher("managerDataS.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
