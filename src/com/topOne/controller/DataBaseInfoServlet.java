package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.topOne.JsonBackData.JsonBackData;
import com.topOne.util.DBUtil;

import net.sf.json.JSONObject;

/**
 * Servlet implementation class DataBaseInfoServlet
 */
@WebServlet("/DataBaseInfoServlet")
public class DataBaseInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DataBaseInfoServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();

		// 需要连接的数据库的相关参数
		Connection connection = null;
		String hostName = request.getParameter("hostName");

		String port = request.getParameter("port");

		String name = request.getParameter("name");

		String password = request.getParameter("password");

		String databasename = request.getParameter("databasename");

		String method = request.getParameter("method");

		String result = "连接失败";

		switch (method) {
		case "ConnectTest":
			String url = "jdbc:mysql://" + hostName + ":" + port + "/" + databasename;
			// 1、加载驱动
			try {
				Class.forName("com.mysql.jdbc.Driver");
				// 2.打开数据库连接
				connection = DriverManager.getConnection(url, name, password);
				System.out.println("连接成功");
				result = "连接成功";
			} catch (ClassNotFoundException e) {

				e.printStackTrace();
			} catch (SQLException e) {

				e.printStackTrace();
			}
			out.print(result);
			break;
		case "ConnectSave":
			hostName = request.getParameter("hostName");
			System.out.println(hostName);

			port = request.getParameter("port");

			name = request.getParameter("name");

			password = request.getParameter("password");

			databasename = request.getParameter("databasename");

			String sourcename = request.getParameter("sourcename");
			System.out.println(sourcename);
			String sourcedescription = request.getParameter("sourcedescription");
			System.out.println(sourcedescription);
			System.out.println(hostName);
			DBUtil dbUtil = new DBUtil();
			String value="";
			if (sourcename!=value && sourcedescription !=value && hostName != value && port != value && name != value
					&& password != value && databasename != value) {
				String sql = "insert into dbconnectinfo(sourcename,sourcedescription,hostName,port,name,password,databasename) values('"
						+ sourcename + "','" + sourcedescription + "','" + hostName + "','" + port + "','" + name
						+ "','" + password + "','" + databasename + "')";

				int intResult = dbUtil.executeUpdate(sql, null);

				if (intResult != -1 | intResult != 0) {
					System.out.println("数据添加成功");
					out.print(JSONObject.fromObject(JsonBackData.success()));
				} else {
					out.print(JSONObject.fromObject(JsonBackData.fail()));
				}
			}
			break;
		default:
			break;
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
