package com.topOne.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.topOne.JsonBackData.JsonBackData;
import com.topOne.bean.DataGroup;
import com.topOne.util.DBUtil;

import javafx.beans.binding.StringBinding;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class DatascGroupServlet
 */
@WebServlet("/DatascGroupServlet")
public class DatascGroupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DatascGroupServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		DBUtil dbUtil = new DBUtil();
		PrintWriter out = response.getWriter(); 
		String id = request.getParameter("id");
		String sql = "select * from source_group where user_id = "+id;
		System.out.println(sql);
		ResultSet rs = dbUtil.executeQuery(sql, null);
		List<DataGroup> list = new ArrayList<>();
		try {
			while (rs.next()) {
				list.add(new DataGroup(rs.getInt(1), rs.getString(2), rs.getInt(3)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			list=null;
		}
		out.print(JSONObject.fromObject(JsonBackData.successRtnObject(list)));
		out.flush();
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
