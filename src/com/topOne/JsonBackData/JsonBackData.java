package com.topOne.JsonBackData;

/**
 * Ajax调用统一返回数据结构
 * 
 *
 */
public class JsonBackData {
	/**
	 * 操作是否成功
	 */
	private int status;
	/**
	 * 操作返回的提示信息
	 */
	private String msg;
	/**
	 * 操作返回的数据
	 */
	private Object data;

	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}


	public Object getData() {
		return data;
	}


	public void setData(Object data) {
		this.data = data;
	}


	public JsonBackData() {
		this.status = 200;
	}

	
	private JsonBackData(int status, String msg, Object data) {
		super();
		this.status = status;
		this.msg = msg;
		this.data = data;
	}
	
	public static JsonBackData success(){
		return new JsonBackData(200, "操作成功", null);
	}
	
	public static JsonBackData success(String msg){
		return new JsonBackData(200, msg, null);
	}
	
	public static JsonBackData successRtnObject(Object data){
		return new JsonBackData(200, "操作成功", data);
	}
	
	public static JsonBackData fail(){
		return new JsonBackData(400, "操作失败", null);
	}
	
	public static JsonBackData fail(String msg){
		return new JsonBackData(400, msg, null);
	}
	
	public static JsonBackData failRtnObject(Object data){
		return new JsonBackData(400, "操作失败", data);
	}
	
}
