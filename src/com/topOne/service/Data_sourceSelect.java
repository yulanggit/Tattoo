package com.topOne.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.topOne.bean.Data_source;
import com.topOne.bean.Source_group;
import com.topOne.bean.Source_groupUser;
import com.topOne.util.DBUtil;
import com.topOne.util.PageUtil;

public class Data_sourceSelect {
	
	//查询所有数据源
	//后期改成查看该用户下的组别名
	public List <Data_source> selectByPage(PageUtil pageUtil){
		DBUtil dbUtil = new DBUtil();
		String sql="select * from Data_source limit ?,?";
	    ResultSet rs = dbUtil.executeQuery(sql, new Object[] {pageUtil.getDataNumber(),pageUtil.getPageSize()});
		List<Data_source> list = new ArrayList<>();

			try {
				while(rs.next()) {
				
						list.add(new Data_source(rs.getInt(1), rs.getString(2),rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getDate(9)));
             }
			} catch (SQLException e) {
				e.printStackTrace();
				list = null;
			}
			return  list;
}
	//删除单条数据源
	public Boolean deleteDatasource(int id) {
		DBUtil dbUtil = new DBUtil();
		Boolean result = true;
		String sql="delete from data_source where id=? ";
		int rs = dbUtil.executeUpdate(sql, new Object[] {id});
		if(rs==-1||rs==0){
			
			result=false;
		
		}
		return result;
	}
	//查询所有类别
	//后期直接搜索该用户下的组别名
	public List<Source_groupUser> selectAllSource_group(){
		DBUtil dbUtil = new DBUtil();
		List<Source_groupUser> list = new ArrayList<>();
		String sql="select s.id,s.group_name,s.user_id,u.name FROM user u,source_group s WHERE u.id=s.user_id;";
		ResultSet rSet = dbUtil.executeQuery(sql, null);
		try {
			while(rSet.next()) {
			
					list.add(new Source_groupUser(rSet.getInt(1),rSet.getString(2),rSet.getInt(3),rSet.getString(4)));
         }
		} catch (SQLException e) {
			e.printStackTrace();
			list = null;
		}
		return  list;
	}
	//删除组类别
	//后期判断在 该用户该组别下是否有文件
	public Boolean deleteSource_group(int id) {
		DBUtil dbUtil = new DBUtil();
		Boolean result = true;
		String sql="delete from source_group where id=? ";
		int rs = dbUtil.executeUpdate(sql, new Object[] {id});
		System.out.println(sql);
		if(rs==-1||rs==0){
			
			result=false;
		
		}
		return result;
	}

	//验证是否可以添加
	  //后期直接看该用户下的组别名是否有相同
    public Boolean checkGroup(String add_groupName) {
         boolean result = false;
    	DBUtil dbUtil = new DBUtil();
    	String sql = "select * from source_group where group_name=?";
    	//System.out.println(sql);
    	ResultSet rs = dbUtil.executeQuery(sql, new Object[] {add_groupName});
    	try {
			result = rs.next();
		} catch (SQLException e) {
		
			
			e.printStackTrace();
			result = false;
		}
    	return result;
    }
	
    //添加分组
    //后期在该用户下添加
    public Boolean addG(String add_groupName1) {
    	
    	DBUtil dbUtil = new DBUtil();
    	String sql="insert into source_group(group_name,user_id) values (?,?)";
    	//后期取用户id
    	int intResult = dbUtil.executeUpdate(sql, new Object[] {add_groupName1,null});
    	if(intResult!=-1) {
			return true;
		}else {
			return false;
		}
    	
    }
}