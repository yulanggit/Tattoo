package com.topOne.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.fs.shell.Count;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.topOne.bean.Data_flow;
import com.topOne.bean.Data_source;
import com.topOne.bean.Data_source1;
import com.topOne.util.DBUtil;

import testjar.UserNamePermission.UserNameMapper;

public class CountAnalysis {
	DBUtil dbUtil = new DBUtil();
	
	/**
	 * 查询source_name中的数据源名称
	 * @param user_id
	 * @return  该用户下的所有数据源集合
	 */
	public List<Data_source1> mrData (String source_type,String user_id) {
		String sql = "select id,source_name from data_source where source_type = ? and user_id = ?";
		ResultSet rSet = dbUtil.executeQuery(sql, new Object[] {source_type,user_id});
		List<Data_source1> list = new ArrayList<>();
		try {
			while(rSet.next()) {
				list.add(new  Data_source1(rSet.getInt(1), rSet.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			list = null;
		}
		return list;
	}
	public List<Data_source> mrData1 (String source_type,String user_id) {
		String sql = "select id,hive_table from data_source where source_type = ? and user_id = ?";
		ResultSet rSet = dbUtil.executeQuery(sql, new Object[] {source_type,user_id});
		List<Data_source> list = new ArrayList<>();
		try {
			while(rSet.next()) {
				list.add(new  Data_source(rSet.getInt(1), rSet.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			list = null;
		}
		return list;
	}
	/**
	 * 查询计算算法名称
	 * @param user_id
	 * @return
	 */
	public List<String> mrAlg (String user_id){
		String sql = "select mr_name from data_flow where user_id = ?";
		ResultSet rSet = dbUtil.executeQuery(sql, new Object[] {user_id});
		List<String> list = new ArrayList<>();
		try {
			while(rSet.next()) {
				list.add(rSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			list = null;
		}
		/*for (Data_flow data_flow : list) {
			list1.add(data_flow.getMr_name());
		}*/
		return list;
	}
	/**
	 * 从data_source表中获取hdfs_path
	 * @param databaseId id
	 * @return  hdfs_path
	 */
	public String hdfs_path(Integer databaseId){
		String sql = "select hdfs_path from data_source where id = ?";
		ResultSet rsSet = dbUtil.executeQuery(sql, new Object[] {databaseId});
		String hdfs_path = "";
		try {
			while(rsSet.next()) {
				hdfs_path =  rsSet.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return hdfs_path;
	}
	/**
	 * 获取当前操作用户group_id
	 * @param databaseId
	 * @return
	 */
	public int group_id(Integer databaseId){
		String sql = "select group_id from data_source where id = ?";
		ResultSet rsSet = dbUtil.executeQuery(sql, new Object[] {databaseId});
		int group_id = 0;
		try {
			while(rsSet.next()) {
				group_id =  rsSet.getInt(1);
			}
			return group_id;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	/**
	 * 根据当前userId获取user_name
	 * @param userId
	 * @return user_name
	 */
	public String userName(String userId) {
		String sql = "select user_name from user where id = ?";
		ResultSet rsSet = dbUtil.executeQuery(sql, new Object[] {userId});
		String userName = "";
		try {
			while(rsSet.next()) {
				userName =  rsSet.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return userName;
	}
	/**
	 * 将处理结果写入data_flow表
	 * @return
	 */
	public boolean writeData_flow(Object[] objects) {
		String sql = "insert into data_flow (flow_name,source_id,flow_type,mr_name,flow_status,result_table,user_id) values(?,?,?,?,?,?,?)";
		int q = dbUtil.executeUpdate(sql, objects);
		if (q != -1) {
			return true;
		}
		return false;
	}
	/**
	 * 将处理结果写入data_source表
	 * @param objects
	 * @return
	 */
	public boolean writeData_source(Object[] objects) {
		String resultsql = "insert into data_source (source_name,source_type,hive_table,user_id,group_id) values(?,?,?,?,?)";
		int q = dbUtil.executeUpdate(resultsql, objects);
		if (q != -1) {
			return true;
		}
		return false;
	}
	
	
	
}
