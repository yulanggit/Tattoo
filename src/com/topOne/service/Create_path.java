package com.topOne.service;

import com.topOne.util.HDFSUtil;
import com.topOne.util.HiveUtil;
import com.topOne.util.PropertiesUtil;


public class Create_path {
	PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
	
	/**
	 * HDFS存放路径
	 * @param path 登录用户名user_name
	 * @return 家目录 /home/bigdata/
	 */
	public String HDFSpath(String path) {
		String hostName = propertiesUtil.readPropertyByKey("hostName");
		HDFSUtil hdfsUtil = new HDFSUtil(hostName);
		hdfsUtil.mkdirs("Tattoo/hive/common/", false);
		hdfsUtil.mkdirs("Tattoo/hive/" + path, false);
		return "/Tattoo/hive/" + path;
		
	}
	/**
	 * 创建HiveDatabase
	 * @param databaseName 
	 * @return 库名
	 */
	public  String HiveBase(String databaseName) {
		HiveUtil hiveUtil = new HiveUtil();
		hiveUtil.createDatabase(databaseName);
		return databaseName;
	}
	
	
}
