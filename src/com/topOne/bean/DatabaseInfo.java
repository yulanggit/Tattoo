package com.topOne.bean;

public class DatabaseInfo {
	private int id;
	private String sourcename;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSourcename() {
		return sourcename;
	}
	public void setSourcename(String sourcename) {
		this.sourcename = sourcename;
	}
	
	public DatabaseInfo() {
	
	}
	public DatabaseInfo(int id, String sourcename) {
		
		this.id = id;
		this.sourcename = sourcename;
	}
	@Override
	public String toString() {
		return "DatabaseInfo [id=" + id + ", sourcename=" + sourcename + "]";
	}
	
	
	
}
