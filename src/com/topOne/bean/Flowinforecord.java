package com.topOne.bean;

public class Flowinforecord {

	public Flowinforecord() {
	}

	public Flowinforecord(Integer id, String user, String database, String sql, String result_path, String create_time,
			String status, String update_time) {
		this.id = id;
		this.user = user;
		this.database = database;
		this.sql = sql;
		this.result_path = result_path;
		this.create_time = create_time;
		this.status = status;
		this.update_time = update_time;
	}

	private Integer id;
	private String user;
	private String database;
	private String sql;
	private String result_path;
	private String create_time;
	private String status;
	private String update_time;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getResult_path() {
		return result_path;
	}

	public void setResult_path(String result_path) {
		this.result_path = result_path;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	@Override
	public String toString() {
		return "flowinforecord [id=" + id + ", user=" + user + ", database=" + database + ", sql=" + sql
				+ ", result_path=" + result_path + ", create_time=" + create_time + ", status=" + status
				+ ", update_time=" + update_time + "]";
	}

}
