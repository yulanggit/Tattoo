package com.topOne.bean;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

// 实现序列化接口
public class Flow implements Writable {
	
	
	public Flow() {
		
	}
	
	public Flow(long upFlow, long downFlow) {
		this.upFlow = upFlow;
		this.downFlow = downFlow;
	}

	private long upFlow;
	private long downFlow;

	public long getUpFlow() {
		return upFlow;
	}

	public void setUpFlow(long upFlow) {
		this.upFlow = upFlow;
	}

	public long getDownFlow() {
		return downFlow;
	}

	public void setDownFlow(long downFlow) {
		this.downFlow = downFlow;
	}

	@Override
	public String toString() {
		return upFlow + "\t" + downFlow + "\t" + (upFlow + downFlow);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.upFlow = in.readLong();
		this.downFlow = in.readLong();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeLong(upFlow);
		out.writeLong(downFlow);
	}
	
	

}
