package com.topOne.bean;

public class Source_groupUser {
  private int id;
  private String group_name;
  private int user_id;
  private String name;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getGroup_name() {
	return group_name;
}
public void setGroup_name(String group_name) {
	this.group_name = group_name;
}
public int getUser_id() {
	return user_id;
}
public void setUser_id(int user_id) {
	this.user_id = user_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
@Override
public String toString() {
	return "Source_groupUser [id=" + id + ", group_name=" + group_name + ", user_id=" + user_id + ", name=" + name
			+ "]";
}
public Source_groupUser(int id, String group_name, int user_id, String name) {
	super();
	this.id = id;
	this.group_name = group_name;
	this.user_id = user_id;
	this.name = name;
}
public Source_groupUser() {
	super();
}
  
	
}
