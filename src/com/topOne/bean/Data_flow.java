package com.topOne.bean;

public class Data_flow {
	private int id ;
	private String flow_name;
	private int source_id;
	private String flow_type;
	private String mr_name;
	private String hive_sql;
	private int flow_status;
	private String result_table;
	private String result_path;
	private int user_id;
	private int dataflowmanager_id;
	
	
	public Data_flow(int id, String flow_name, int source_id, String flow_type, String mr_name, String hive_sql,
			int flow_status, String result_table, String result_path, int user_id, int dataflowmanager_id) {
		super();
		this.id = id;
		this.flow_name = flow_name;
		this.source_id = source_id;
		this.flow_type = flow_type;
		this.mr_name = mr_name;
		this.hive_sql = hive_sql;
		this.flow_status = flow_status;
		this.result_table = result_table;
		this.result_path = result_path;
		this.user_id = user_id;
		this.dataflowmanager_id = dataflowmanager_id;
	}
	public Data_flow(int source_id) {
		this.source_id = source_id;
	}
	public Data_flow(String mr_name) {
		this.mr_name = mr_name;
	}
	
	public Data_flow(int id, String flow_name, int source_id, String flow_type, String mr_name, String hive_sql,
			int flow_status, String result_table, String result_path, int user_id) {
		this.id = id;
		this.flow_name = flow_name;
		this.source_id = source_id;
		this.flow_type = flow_type;
		this.mr_name = mr_name;
		this.hive_sql = hive_sql;
		this.flow_status = flow_status;
		this.result_table = result_table;
		this.result_path = result_path;
		this.user_id = user_id;
	}
	
	public Data_flow () {
		
	}
	
	
	public int getDataflowmanager_id() {
		return dataflowmanager_id;
	}
	public void setDataflowmanager_id(int dataflowmanager_id) {
		this.dataflowmanager_id = dataflowmanager_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFlow_name() {
		return flow_name;
	}
	public void setFlow_name(String flow_name) {
		this.flow_name = flow_name;
	}
	public int getSource_id() {
		return source_id;
	}
	public void setSource_id(int source_id) {
		this.source_id = source_id;
	}
	public String getFlow_type() {
		return flow_type;
	}
	public void setFlow_type(String flow_type) {
		this.flow_type = flow_type;
	}
	public String getMr_name() {
		return mr_name;
	}
	public void setMr_name(String mr_name) {
		this.mr_name = mr_name;
	}
	public String getHive_sql() {
		return hive_sql;
	}
	public void setHive_sql(String hive_sql) {
		this.hive_sql = hive_sql;
	}
	public int getFlow_status() {
		return flow_status;
	}
	public void setFlow_status(int flow_status) {
		this.flow_status = flow_status;
	}
	public String getResult_table() {
		return result_table;
	}
	public void setResult_table(String result_table) {
		this.result_table = result_table;
	}
	public String getResult_path() {
		return result_path;
	}
	public void setResult_path(String result_path) {
		this.result_path = result_path;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	@Override
	public String toString() {
		return id + "," + flow_name + "," + source_id + ","
				+ flow_type + "," + mr_name + "," + hive_sql + "," + flow_status
				+ "," + result_table + "," + result_path + "," + user_id;
	}
	
	
	
}
