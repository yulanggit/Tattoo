package com.topOne.bean;

public class DataGroup {

	public DataGroup() {

	}

	public DataGroup(int id, String group_name, int user_id) {
		this.id = id;
		this.group_name = group_name;
		this.user_id = user_id;
	}

	private int id;
	private String group_name;
	private int user_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	@Override
	public String toString() {
		return "DataGroup [id=" + id + ", group_name=" + group_name + ", user_id=" + user_id + "]";
	}

}
