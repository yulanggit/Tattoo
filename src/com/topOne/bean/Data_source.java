package com.topOne.bean;

import java.util.Date;

public class Data_source {
	
	public Data_source(int id, String hive_table) {
		this.id = id;
		this.hive_table = hive_table;
	}
	
	private int id;
	private String source_name;
	private String source_type;
	private String hdfs_path;
	private String hive_table;
	private String table_column;
	private int user_id;
	private int group_id;
	private Date create_date;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSource_name() {
		return source_name;
	}
	public void setSource_name(String source_name) {
		this.source_name = source_name;
	}
	public String getSource_type() {
		return source_type;
	}
	public void setSource_type(String source_type) {
		this.source_type = source_type;
	}
	public String getHdfs_path() {
		return hdfs_path;
	}
	public void setHdfs_path(String hdfs_path) {
		this.hdfs_path = hdfs_path;
	}
	public String getHive_table() {
		return hive_table;
	}
	public void setHive_table(String hive_table) {
		this.hive_table = hive_table;
	}
	public String getTable_column() {
		return table_column;
	}
	public void setTable_column(String table_column) {
		this.table_column = table_column;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getGroup_id() {
		return group_id;
	}
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	@Override
	public String toString() {
		return "Data_source [id=" + id + ", source_name=" + source_name + ", source_type=" + source_type
				+ ", hdfs_path=" + hdfs_path + ", hive_table=" + hive_table + ", table_column=" + table_column
				+ ", user_id=" + user_id + ", group_id=" + group_id + ", create_date=" + create_date + "]";
	}
	public Data_source(int id, String source_name, String source_type, String hdfs_path, String hive_table,
			String table_column, int user_id, int group_id, Date create_date) {
		super();
		this.id = id;
		this.source_name = source_name;
		this.source_type = source_type;
		this.hdfs_path = hdfs_path;
		this.hive_table = hive_table;
		this.table_column = table_column;
		this.user_id = user_id;
		this.group_id = group_id;
		this.create_date = create_date;
	}
	
	public Data_source(int id, String source_name, String source_type, String table_column, Date create_date) {
		super();
		this.id = id;
		this.source_name = source_name;
		this.source_type = source_type;
		this.table_column = table_column;
		this.create_date = create_date;
	}
	public Data_source() {
		super();
	}
	
}
