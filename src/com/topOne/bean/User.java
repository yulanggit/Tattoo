package com.topOne.bean;

public class User {
	private int id;

	private String user_name;
	private String user_pwd;
	private String name;
	
	
	public User() {
		
	}
	
	public User(int id, String user_name, String user_pwd, String name) {
		this.id = id;
		this.user_name = user_name;
		this.user_pwd = user_pwd;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_pwd() {
		return user_pwd;
	}
	public void setUser_pwd(String user_pwd) {
		this.user_pwd = user_pwd;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", user_name=" + user_name + ", user_pwd=" + user_pwd + ", name=" + name + "]";
	}
	
	
	
	
	
	
}
