package com.topOne.util;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PageUtil {

	public PageUtil() {
		
	}

	/**
	 * 不使用默认的分页大小
	 * @param pageSize
	 * @param pageNumber
	 */
	public PageUtil(Integer pageSize, Integer pageNumber,String tableName) {
		this.pageSize = pageSize;
		this.pageNumber = pageNumber;
		getTotal(tableName);
	}

	/**
	 * 使用默认的分页大小进行分页
	 * @param pageNumber
	 */
	public PageUtil(Integer pageNumber,String tableName) {
		this.pageNumber = pageNumber;
		getTotal(tableName);
	}

	// 每页的数据条数
	private Integer pageSize = 3;
	// 当前页数
	private Integer pageNumber = 1;
	// 当前页数据起始索引
	private Integer dataNumber;
	// 获得总页数
	private Integer totalPage;

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getDataNumber() {
		return (this.pageNumber - 1) * this.pageSize;
	}

	public void setDataNumber(Integer dataNumber) {
		this.dataNumber = dataNumber;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}
	
	public void getTotal(String tableName) {
		DBUtil dbUtil = new DBUtil();
		String sql = "select count(*) from " + tableName;
		ResultSet rs = dbUtil.executeQuery(sql, null);
		Integer count = 0;
		try {
			rs.next();
			count = rs.getInt(1);
			this.totalPage = count % pageSize == 0 ? count / pageSize : (count / pageSize + 1);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
}
