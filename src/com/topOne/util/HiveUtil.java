 package com.topOne.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class HiveUtil {
	
	private Statement statement = null;
	
	public HiveUtil() {
		
		open();
		
		//init();
	}
	
	static {
		try {
			// 1.加载驱动
			Class.forName("org.apache.hive.jdbc.HiveDriver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void open() {
		try {
			PropertiesUtil propertiesUtil = new PropertiesUtil("system.properties");
			String hostName = propertiesUtil.readPropertyByKey("hostName");
			// 2.打开连接
			Connection connection = DriverManager.getConnection("jdbc:hive2://" + hostName + ":10010/");
			// 3.获得操作对象 - 会话
			statement = connection.createStatement();
			System.out.println("Hive成功登录");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void init() {
		try {
			statement.execute("create temporary function sub as 'com.sand.udf.SubString'");
			statement.execute("create temporary function jsonParse as 'com.sand.udf.JsonParse'");
			statement.execute("create temporary function split1 as 'com.sand.udtf.Split'");
			statement.execute("create temporary function concat1 as 'com.sand.udaf.Concat'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 创建数据库 - 用户注册时调用
	 * @param databaseName 根据用户标识生成的数据库名称
	 */
	public void createDatabase(String databaseName) {
		try {
			statement.execute("create database " + databaseName);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void createCommondatabase(String databaseName) throws SQLException {
		statement.execute("create database " + databaseName);
	}
	/**
	 * 删除数据库 - 用户注销时调用
	 * @param databaseName 根据用户标识生成的数据库名称
	 */
	public void deleteDatabase(String databaseName) {
		try {
			statement.execute("drop database if exists " + databaseName);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 获取数据库名称
	 * @return
	 */
	public List<String> getDatabaseList() {
		List<String> list = new ArrayList<>();
		try {
			ResultSet rs = statement.executeQuery("show databases");
			while (rs.next()) {
				list.add(rs.getString(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * 删除表
	 * @param databaseName 根据用户标识生成的数据库名称
	 */
	public void deletetable(String tableName) {
		try {
			statement.execute("drop table " + tableName);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 切换数据库 - 只对当前会话有效
	 * @param databaseName 目标数据库名称
	 */
	public String changeDatabase(String databaseName) {
		try {
			statement.execute("use " + databaseName);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return databaseName;
	}
	
	/**
	 * 获得当前数据库中的数据列表 - 注意切换数据库
	 * @return 数据表名称的集合
	 */
	public List<String> getTaleList() {
		List<String> list = new ArrayList<>();
		try {
			ResultSet rs = statement.executeQuery("show tables");
			while (rs.next()) {
				list.add(rs.getString(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * 获得数据表的简要信息
	 * @param tableName 数据表名称
	 * @return 列名及列的数据类型
	 */
	public List<String> getTableInfo(String tableName){
		List<String> list = new ArrayList<>();
		try {
			ResultSet rs = statement.executeQuery("desc " + tableName);
			while (rs.next()) {
				list.add(rs.getString(1) + "\t" + rs.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * 获取数据表所有的预览数据
	 * @param tableName 数据表名称
	 * @return 数据表预览数据
	 */
	public List<String> getTableDataAll(String tableName){
		List<String> list = new ArrayList<>();
		try {
			int size = getTableInfo(tableName).size();
			ResultSet rs = statement.executeQuery("select * from " + tableName);
			while (rs.next()) {
				String line = "";
				for(int i = 1;i <= size;i ++) {
					line += rs.getString(i) + "\t";
				}
				list.add(line);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			list = null;
		}
		return list;
	}
	/**
	 * 获取数据表前十条的预览数据
	 * @param tableName 数据表名称
	 * @return 数据表预览数据
	 */
	public List<String> getTableData(String tableName){
		List<String> list = new ArrayList<>();
		try {
			int size = getTableInfo(tableName).size();
			ResultSet rs = statement.executeQuery("select * from " + tableName +" limit 10");
			while (rs.next()) {
				String line = "";
				for(int i = 1;i <= size;i ++) {
					line += rs.getString(i) + "\t";
				}
				list.add(line);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			list = null;
		}
		return list;
	}
	
	/**
	 * 获取数据表前三条的预览数据
	 * @param tableName 数据表名称
	 * @return 数据表预览数据
	 */
	public List<String> getTableData3(String tableName){
		List<String> list = new ArrayList<>();
		try {
			int size = getTableInfo(tableName).size();
			ResultSet rs = statement.executeQuery("select * from " + tableName +" limit 3");
			while (rs.next()) {
				String line = "";
				for(int i = 1;i <= size;i ++) {
					line += rs.getString(i) + "\t";
				}
				list.add(line);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			list = null;
		}
		return list;
	}
	
	/**
	 * 获得查询sql执行后的返回结果
	 * @param sql 用户自定义sql
	 * @return sql执行结果集中的所有数据
	 */
	public List<String> getResultData(String sql,String tableName){
		List<String> list = new ArrayList<>();
		// String databaseName = "data_flow";
		// 生成一个对于当前流程唯一的中间表名称
		// 如果流程会反复执行则先删除该表再创建
		// String tableName = "data_flow_1";
		sql = "create table " + tableName + " as " + sql;
		try {
			// 执行查询语句，同时使用一个表进行记录
			statement.execute(sql);
			// 获得中间表的列信息 - 取决于用户执行sql的结果集结构
			int size = getTableInfo(tableName).size();
			ResultSet rs = statement.executeQuery("select * from " + tableName);
			while (rs.next()) {
				String line = "";
				for(int i = 1;i <= size;i ++) {
					line += rs.getString(i) + "\t";
				}
				list.add(line);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * 新建表--注意先切换数据库
	 * @param tableName
	 * @param columnName1 
	 * @param columnType1
	 * @param columnName2
	 * @param columnType2
	 * @param columnName3
	 * @param columnType3
	 * @return
	 */
	public boolean mkTable(String tableName,String columnName1,String columnType1,String columnName2,String columnType2,String columnName3,String columnType3,String columnName4,String columnType4) {
		String sql = "create table " + tableName + "(" + columnName1 + " " + columnType1 + "," + columnName2 + " " + columnType2 + "," + columnName3 + " " + columnType3 + "," + columnName4 + " " + columnType4+")" + " " + "row format delimited fields terminated by '\\t'";
		System.out.println(sql);
		try {
			statement.execute(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	/**
	 * 新建表--注意先切换数据库
	 * @param tableName
	 * @param columnName1
	 * @param columnType1
	 * @param columnName2
	 * @param columnType2
	 * @return
	 */
	public boolean mkTable(String tableName,String columnName1,String columnType1,String columnName2,String columnType2) {
		String sql = "create table " + tableName + "(" + columnName1 + " " + columnType1 + "," + columnName2 + " " + columnType2+")" + " " + "row format delimited fields terminated by '\\t'";
		System.out.println(sql);
		try {
			statement.execute(sql);
			return true;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	/**
	 * 向Hive数据仓库的表中导入数据 --先切换数据库
	 * @param inpath 输入路径
	 * @param tableName 导入的表名称
	 * @return boolean
	 */
	public boolean load(String inpath,String tableName) {
		String sql = "load data inpath '" + inpath + "' into table " + tableName ;
		try {
			statement.execute(sql);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	//执行sql语句
	public boolean execute(String sql) {
		boolean result=false;
		try {
			statement.execute(sql);
			result=true;
		} catch (SQLException e) {
			e.printStackTrace();
			result=false;
		}
		return result;
		
	}
	
	/**
	 * 根据字段和sql语句获得所查询的列的信息
	 * @param sql  
	 * @param column  //字段信息
	 * @return
	 */
	
	public List<String> getSomeColumnData(String sql,String column){
		List<String> list = new ArrayList<>();
		try {
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				String line = "";
				for(int i = 1;i <= column.split(",").length;i ++) {
					line += rs.getString(i) + "\t";
				}
				list.add(line);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			list = null;
		}
		return list;
	}
	

}
