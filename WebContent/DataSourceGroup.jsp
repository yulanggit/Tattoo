<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>数据源分组</title>
<link rel="stylesheet" type="text/css" href="css/button.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/DataSourceGroup.js"></script>
<%@ include file="./main.jsp"%>
</head>
<body>
	<div style='position: fixed; left: 300px; top: 100px; z-index：1000; height: 50px;'>
		<h1>数据源分组</h1>
		<br>
		<br>
		<select id="selectGroup" name="group_name"></select> 
		<input id="newGroup" class="button" type="button" value="新增"> 
		<input id="deleteGroup" class="button" type="button" value="删除"> 
		<input id="updateGroup" class="button" type="button" value="修改">
		<div id="NewGroup"></div>
	</div>
</body>
</html>