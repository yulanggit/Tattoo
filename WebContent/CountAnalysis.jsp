<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/count.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/countAnalysis.js"></script>
<!-- <script type="text/javascript" src="js/countDemo.js"></script> -->
<%@include file="./main.jsp"%>
<title>统计分析</title>
</head>
<body>
	<div class="div" style="position: fixed; left: 300px; top: 100px; z-index: 1000; height: 50px">
		<span>统计分析</span> <select name="ways" >
			<option>选择分析方式</option>
			<option value="MR">MR</option>
			<option value="HQL">HQL</option>
		</select> <input type="button" value="确定" class="getways" /><br />


		<div id="clear">
			<div id="showmr" style="display: none">
				<!-- 选择数据源 -->
				<span>数&nbsp;据&nbsp;源&nbsp;</span> <select id='database'></select><br />
				<!-- 指定算法 -->
				<span>指定算法</span> <select id='alg' name="algorithm">
					<option value="">请指定算法</option>
					<option value="FlowCount">流量统计</option>
					<option value="WordCount">单词计数</option>
				</select><br />
				<!-- 自定义结果路径 -->
				<span>结果表名</span> <input type="text" name="resultTablename"
					placeholder="请自定义结果表名" /><br />
				<!-- 保存计算流程 -->
				<!-- <br /> <span>是否保存计算流程</span><input type="checkbox" id="check" name="auto" 
					 /><br />
				 -->
				 <!-- 指定流程名称 -->
				 <span>流程名称&nbsp;</span><input type="text" name="flowName" placeholder="请自定义流程名称"/><br />
				<!-- 提交数据 -->
				<input type="button" value="开始计算" class="start" />
				
				<!-- 右边显示内容，使用js追加 -->
				<!-- <div id="MRright">
					<div id="MRtable">
						<s style="font-size: 24px;margin-left: 230px;">计算结果</s><br />
					</div>
				</div> -->
			</div>

			<div id="showhql" style="display: none">
				<!-- 选择数据源 -->
				<span>数&nbsp;据&nbsp;源&nbsp;</span> <select id='database1' name="database"></select>&nbsp;
				<input type="button" value="预览" class="showTable"/><br />
				
				<!-- 结果表名称 -->
				<span>结&nbsp;果&nbsp;表&nbsp;名&nbsp;称&nbsp;&nbsp;</span> <input type="text" name="resultTable"
					placeholder="请自定义结果表名称" /><br />
				
				<!-- 常用统计功能 -->
				<!-- <span>常用&nbsp;统&nbsp;计&nbsp;功&nbsp;能</span> <select name="common">
					<option value=""></option>
					<option value="max">求最大值</option>
					<option value="min">求最小值</option>
					<option value="average">求平均值</option>
					<option value="sum">计算总和</option>
				</select><br /> -->
				
				<!-- 自定义SQL -->
				<textarea class="ownsql" name="sql" placeholder="请输入正确的查询SQL语句" rows="5" cols="10" style="width: 310px;
						height: 34px;"></textarea>
				
				<!-- 提交数据 -->
				<br /><input type="button" value="开始计算" class="begin" />
				<input class="echarts" type="button" value="查看Echarts图" style="margin-left: 16px;" />
				<div align="center" id="demo">
					<div id="tableHead"> 
						<!-- 预览数据 -->
					</div>	
					<div style="height:40px;width:370px;overflow: auto;">
						<div id="demo2">
							<!-- 内容滚动 -->
						</div>
					</div>
				</div>
				<!-- 右边显示内容，使用js追加 -->
				<div id="right">
					<div id="table">
						<s style="font-size: 24px;margin-left: 230px;">计算结果</s><br />
					</div>
				</div>
					<div id="zt" style='position: fixed; left: 300px; top: 425px; z-index：1000; height: 50px;display: none;' >
		<table class="tbodyList table table-striped table-bordered table-advance table-hover">
			<tr>
				<th><span class="hidden-phone">id</span></th>
				<th><span class="hidden-phone">flow_name</span></th>
				<th><span class="hidden-phone">source_id</span></th>
				<th><span class="hidden-phone">flow_type</span></th>
				<th><span class="hidden-phone">hive_sql</span></th>
				<th><span class="hidden-phone">flow_status</span></th>
				<th><span class="hidden-phone">result_table</span></th>
				<th><span class="hidden-phone">user_id</span></th>
				<th><span class="hidden-phone">update_time</span></th>
			</tr>
			<tbody id="dataflowstatus" class='tbodyList'>
										
		</tbody>
		</table>
	</div>
			</div>
		</div>
		
		
	</div>
	
	
</body>
</html>