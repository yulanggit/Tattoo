<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String path=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath();%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>数据源管理</title>
<!-- 引入reset.css(通用的格式化样式文件) -->
<link rel="stylesheet" type="text/css" href="<%=path %>/css/reset.css">
<!-- 引入控制当前页面的样式文件 -->
<link rel="stylesheet" type="text/css" href="<%=path %>/css/list.css">
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/source_group.js"></script>
<script type="text/javascript">
    var path = '<%=path%>';
	// 如果数据为数值类型，不需要加单引号
	var pageNumber = ${page.pageNumber};
	// 如果数据为字符串类型，必须加单引号
	// el表达式取值时可以直接执行方法，或者取出属性(必须和实体类当中的写法完全一致->对应上封装方法)
	
</script>
</head>
<h1>类别管理</h1>
<div>
<table>
	<!-- 表头 -->
	<thead>
		<tr>
			<td>行号</td>
			<td>类别</td>
			<td>所属用户</td>
			<td>操作</td>
		</tr>
	</thead>
	<!-- 身体 -->
	<tbody>
		<c:forEach items="${list1}" var="source_type" varStatus="status" >
			<tr id="tr_${source_type.id}"  <c:if test="${status.index % 2 == 0}">class="one"</c:if><c:if test="${status.index % 2 != 0}">class="two"</c:if>>
				<td>${status.index + 1 }</td>
				<td><center><input type="text" name="group_name" class="group_name" value="${source_type.group_name}"/>
				
				</center></td>
				<td>${source_type.name}
				
				</td>
				<td>&nbsp;&nbsp;
				<a href="javascript:" onclick="updata(${source_type.id})">修改</a>&nbsp;&nbsp;
				<a href="javascript:" onclick="addGroup()">添加</a>&nbsp;&nbsp;
				<a href="javascript:" onclick="deletes(${source_type.id})">删除</a>&nbsp;&nbsp;
				</td>
			</tr>
		</c:forEach>
	</tbody>

</table>
</div>
<br/><br/>
<form method="post">
<div id="addGroup" style="display:none">
    <input type="text" name="add_groupName" placeholder="请输入组别名"/><span id="mark"></span>&nbsp;&nbsp;
     
    <input type="button" value="添加" id="addG"/>
  
</div>
 </form>  
<body>

</body>
</html>