<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/LoadHdfs.js"></script> 
<%@ include file="./main.jsp"%>
<script type="text/javascript">
	var outcome = "<%=request.getParameter("outcome")%>";
	if(outcome!='null'){
		if(outcome=='true'){
			alert("上传成功！")
		}else{
			alert("上传失败！")
		}
	}
</script>
<title>文件上传</title>
</head>
<body>
<div style="position: fixed; left: 300px; top: 100px; z-index: 1000; height: 50px">
<h4>新建数据源</h4>
<form action="LoadHdfsServlet" method="post" enctype="multipart/form-data">
	<span>名称:</span>
	<input type="text" name="sourceName"  placeholder="数据源名称" /><br/>
	<span>分组名:</span>
	<select id="selectGroup" name="group_name"></select> <br />
	<span>自定义文件名:</span>
	<input type="text" name="zdifile" placeholder="文件名称" /><br />
	<input type="hidden" name="id" id="userId"/>
	<input type="hidden" name="userName" id="userName"/>
	
	<input type="file" name="data"/><br />
	<span>上传目标：</span>
	<select name="targetDir">
		<option value="commonSpace">公共空间</option>
		<option value="ownSpace">个人空间</option>
	</select><br/>
	
	<span>自定义路径:</span>
	<input type=text name="zdy" placeholder="例如：home/bigdata/"/><br/>
	<input  type="submit" class="pass" value="上传" />
</form>
</div>
</body>
</html>