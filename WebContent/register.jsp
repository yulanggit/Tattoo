<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户注册</title>
<!-- <link href="css/register.css" rel="stylesheet" /> -->
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/register.js"></script>
<link rel="shortcut icon" href="../favicon.ico">
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/animate-custom.css" />
</head>
<body>
<form action="RegisterServlet" method="post" >
<div class="container" id="show">
		<header>
		<section>
		<div id="container_demo">
			<a class="hiddenanchor" id="toregister"></a> <a class="hiddenanchor"
				id="tologin"></a>
			<div id="wrapper">
				<div id="login" class="animate form">
					<form  action="LoginServlet" method="post">
						<h1>Register</h1>
						<p>
							<label for="username" class="uname"> 登录账号:</label> 
							<input id="1" name="user_name" class="listen" required="required" type="text" placeholder="请输入您的账号" />
							<span style="font-size:10px;margin-left:70px"> </span>
						</p>
						<p>
							<label for="password" class="youpasswd">登录密码: </label> 
							<input  id="2" name="user_pwd" class="listen" required="required" type="password" placeholder="请输入您的密码" />
							<span style="font-size:10px;margin-left:70px"> </span>
						</p>
						<p>
							<label for="password" class="youpasswd">用户昵称: </label> 
							<input  id="3"  type="text" name="name" class="listen" required="required" placeholder="请输入您的用户昵称" />
						</p>
						<p> 
							<label for="password" class="youpasswd">用户身份: </label> 
							<select name="type">
								<option value="Java开发工程师">Java开发工程师</option>
								<option value="大数据开发工程师">大数据开发工程师</option>
								<option value="Phyon开发工程师">Phyon开发工程师</option>
								<option value="其他">其他</option>
							</select>
						</p>
						<p>
							<label for="password" class="youpasswd">出生年月: </label> 
							<input  type="date" name="birthday" value=""  required="required"  />
						</p>
						<p class="bottomButton login button">
							<input type="reset" style='float: left' value="重置" /> 
							<input id="bt" type="button" value="注册" class="register"/>
						</p>
						<p class="change_link">
							Join us <a href="tel:1866530xxxx" class="to_register">Tattoo：1866530XXXX</a>
						</p>
					</form>
				</div>
			</div>
		</div>
		</section>
	</div> 





<!-- <div id="show">
	<h1 id="user">用户注册</h1>
	<div id="zhuti">
	<div>
	<label >登录账号:</label>
		<input id="1" type="text" name="user_name" class="listen"/>
		<br />
		<span style="font-size:10px;margin-left:70px"> </span>
		</div>
		<div>
	<label>登录密码:</label>
		<input id="2" type="password" name="user_pwd" class="listen"/><br />
		<span style="font-size:10px;margin-left:70px"> </span>
		</div>
	<span>用户昵称:</span>
		<input id="3" type="text" name="name" class="listen"/><br /><br />
	<span>用户身份:</span>
		<select name="type">
			<option value="Java开发工程师">Java开发工程师</option>
			<option value="大数据开发工程师">大数据开发工程师</option>
			<option value="Phyon开发工程师">Phyon开发工程师</option>
			<option value="其他">其他</option>
		</select><br /><br />
	<span>出生年月:</span>
		<input type="date" name="birthday" value="" /><br /><br />
	</div>
	<div id="qq">
		<input type="reset" value="重置"/>
		<input id="bt" type="button" value="注册" class="register"/>
	</div>
	</div> -->
	</form>
</body>
</html>