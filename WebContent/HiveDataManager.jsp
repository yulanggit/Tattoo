<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>数据源编辑</title>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
	var source_id = "<%=request.getParameter("source_id")%>";
</script>
<script type="text/javascript" src="js/HiveDataManager.js"></script>
<%@ include file="./main.jsp"%>
</head>
<body>
	<div style='position: fixed; left: 300px; top: 100px; z-index：1000; height: 50px;'>
		<div id="hive" style="display:none;">
			<h4>hive数据源编辑</h4>
			<form action="HiveDataManagerServlet" method="post" enctype="multipart/form-data">
				<input type="hidden" name="id" id="userId" /> 
				<input type="hidden" name="userName" id="userName" /> 
				<input type="hidden" name="tableName" id="tableName" />
				<input type="hidden" name="method" value="hivefile" /> <br> 
				<span>数据上传方式</span>
				<select name="inputtype" id="parenthive">
					<option class="hivefile" value='0'>请选择上传方式</option>
					<option class="hivefile" value='1'>文件上传</option>
					<option class="hivetable" value='2'>数据源拉取</option>
				</select>
				
				<!--文件上传-->
				<div id="hivefileinput" style="display:none;">
					<input type="file" name="data"><br /> 
					<input class="group" type="hidden" name="groupname" value=" " /> 
					<label><input type="checkbox" name="overwrite" value="overwrite" />是否覆盖导入</label>
					<div class="columnInfo"></div>
					<input type="submit" value="上传" />
				</div>
				
				<!-- 数据源拉取  -->
				<div id="hivetableinput" style="display:none;">
					<span>数据库:</span>
					<select id='database'>
						<option value=''>请选择数据库  </ option>
					</select>
					<span>数据表:</span>
					<select id='table'>
					</select><br />
					<span>单表导入/sql导入:</span>
					<select id='import'>
						<option value=''>请选择数据库  </ option>
						<option value="Single" class="1">单表导入</option>
						<option value="Sql" class="2">Sql导入</option>
					</select><br />
					<div id="SingleTable" style="display :none">
						<label><input type="checkbox" name="overwrite" value="overwrite"/>是否覆盖导入</label><br /><br />
						<input type="button" class="import" value="导入">
					</div>
					<div id="SqlTable" style="display :none">
	    				<textarea rows="5" cols="20" id="sqlcustom" name="sqlcustom" style="width :300px " placeholder="自定义sql语句 :例：select * from data_source" ></textarea><br/>
						<label><input type="checkbox" name="overwrite" value="overwrite"/>是否覆盖导入</label><br />
						<input type="button" class="sqlimport" value="导入"/>
					</div>
				</div>
				
			</form>
		</div>
	
		<div id="hdfs" style="display:none;">
			<h4>HDFS数据源编辑</h4>
			<span>数据上传方式</span>
			<select name="inputtype" id="parenthive1">
				<option class="hdfsfile" value='0'>请选择上传方式</option>
				<option class="hdfsfile" value='1'>文件上传</option>
				<option class="hdfstable" value='2'>数据源拉取</option>
			</select>
			
			<!--文件上传-->
			<div id="hdfsfileinput" style="display:none;">
				<form action="HiveDataManagerServlet" method="post" enctype="multipart/form-data">
					<input type="hidden" name="id" id="userId"/>
					<input type="hidden" name="userName" id="userName"/>
					<input type="hidden" name="hdfs_path" id="hdfs_path"/>
					<input type="hidden" name="method" value="hdfsfile" /> <br> 
					<input type="file" name="data"/><br />
					<label><input type="checkbox" name="overwrite" value="overwrite"/>是否覆盖导入</label><br />
					<input  type="submit" class="pass" value="上传" />
				</form>
			</div>
			
			<!-- 数据源拉取  -->
			<div id="hdfstableinput" style="display:none;">
				<span>数据库:</span>
				<select id='database1'>
					<option value=''>请选择数据库  </ option>
				</select>
				<span>数据表:</span>
				<select id='table1'></select><br /><br/>
				<span>单表导入/sql导入:</span>
				<select id='import1'>
					<option value=''>请选择导入方式  </ option>
					<option value="Single" class="1">单表导入</option>
					<option value="Sql" class="2">Sql导入</option>
				</select><br />
				<div id="SingleTable1" style="display :none">
					<label><input type="checkbox" name="overwrite1" value="overwrite"/>是否覆盖导入</label><br />
					<input type="button" class="import1" value="导入">
				</div>
				
				<div id="SqlTable1" style="display :none">
				    <textarea rows="5" cols="20" id="sqlcustom1" name="sqlcustom1" style="width :300px " placeholder="自定义sql语句 :例：select * from data_source" ></textarea><br/>
					<label><input type="checkbox" name="overwrite2" value="overwrite2"/>是否覆盖导入</label><br />
					<input type="button" class="sqlimport1" value="导入"/>
				</div>
			</div>
		</div>
	</div>
</body>
</html>