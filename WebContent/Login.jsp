<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="Login and Registration Form with HTML5 and CSS3" />
<meta name="keywords"
	content="html5, css3, form, switch, animation, :target, pseudo-class" />
<meta name="author" content="Codrops" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登录界面</title>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/Login.js"></script>
<link rel="shortcut icon" href="../favicon.ico">
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/animate-custom.css" />
</head>
<body>
	<!-- 	<form action="LoginServlet" method="post">
		<span>帐号:</span><input type="text" name="username"><br> <span>密码:</span><input
			type="password" name="password"><br> <input
			type="submit" value="登录">
	</form> -->
	<div class="container">
		<header>
		<h1>大数据管理系统</h1>
		<nav class="codrops-demos"> </nav> </header>
		<section>
		<div id="container_demo">
			<a class="hiddenanchor" id="toregister"></a> <a class="hiddenanchor"
				id="tologin"></a>
			<div id="wrapper">
				<div id="login" class="animate form">
					<form  action="LoginServlet" method="post">
						<h1>Log in</h1>
						<p>
							<label for="username" class="uname"> 账号 </label> 
							<input id="username" name="username" required="required" type="text" placeholder="请输入您的账号" />
						</p>
						<p>
							<label for="password" class="youpasswd">密码 </label> 
							<input id="password" name="password" required="required" type="password" placeholder="请输入您的密码" />
						</p>
						<p class="bottomButton login button">
							<input type="reset" style='float: left' value="重置" /> 
							<input type='button' value="登陆" />
						</p>
						
						<p class="change_link">
							<a href="/Tattoo/register.jsp" style="position:absolute;left:10px;top:15px;">注册</a>
							Join us 
							<a href="tel:1866530xxxx" class="to_register">Tattoo：1866530XXXX</a>
						</p>
					</form>
				</div>
			</div>
		</div>
		</section>
	</div>
</body>
</html>