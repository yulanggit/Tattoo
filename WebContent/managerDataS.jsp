<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String path=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath();%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>数据源管理</title>
<!-- 引入reset.css(通用的格式化样式文件) -->
<link rel="stylesheet" type="text/css" href="<%=path %>/css/reset.css">
<!-- 引入控制当前页面的样式文件 -->
<link rel="stylesheet" type="text/css" href="<%=path %>/css/list.css">
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/managerDataS.js"></script>
<script type="text/javascript">
    var path = '<%=path%>';
	// 如果数据为数值类型，不需要加单引号
	var pageNumber = ${page.pageNumber};
	// 如果数据为字符串类型，必须加单引号
	// el表达式取值时可以直接执行方法，或者取出属性(必须和实体类当中的写法完全一致->对应上封装方法)
	var data_SourceName ='${list.get(0).getSource_name()}'
</script>
</head>


<body>
<center>
<br/><br/>
<h1>数据源管理</h1>
<div id="showData_source" >
<div id="select">
<select name="source_type" class="source_data">
<option value="all">全部分类</option>
<option value="mysql">mysql</option>
<option value="oracle">oracle</option>
</select>
<a href="javascript:" onclick="typeManager()">类别管理</a>
</div>
<div id="tableShow">
<table>
	<!-- 表头 -->
	<thead>
		<tr>
			<td>行号</td>
			<td>名称</td>
			<td>数据源组件</td>
			<td>HDFS路径</td>
			<td>HDFS表名</td>
			<td>上传时间</td>
			<td>操作</td>
		</tr>
	</thead>
	<!-- 身体 -->
	<tbody>
		<c:forEach items="${list}" var="data_source" varStatus="status" >
			<tr id="tr_${data_source.id}"  <c:if test="${status.index % 2 == 0}">class="one"</c:if><c:if test="${status.index % 2 != 0}">class="two"</c:if>>
				<td>${status.index + 1 }</td>
				<td><center><input type="text" name="source_name" class="source_name"  value="${data_source.source_name}"/>
				</center></td>
				<td><center><input type="text" name="source_type" class="source_type" value="${data_source.source_type}"/>
				</center></td>
				<td><center><input type="text" name="hdfs_path" class="hdfs_path" value="${data_source.hdfs_path}"/>
				</center></td>
				<td><center><input type="text" name="hive_table" class="hive_table" value="${data_source.hive_table}"/>
				</center></td>
				<td>${data_source.create_date}</td>
				<td><a href="javascript:" onclick="updata(${data_source.id})">修改</a>&nbsp;&nbsp;
				<a href="">查看</a>&nbsp;&nbsp;
				<a href="javascript:" onclick="deletes(${data_source.id})">删除</a>&nbsp;&nbsp;
				</td>
			</tr>
		</c:forEach>
	</tbody>
	<!-- 结尾 -->
	<tfoot>
		<tr>
			<!-- 跨列合并 -->
			<td colspan="2">
				<span style="display: ${page.pageNumber - 1 == 0 ? '' : 'none' }">已是首页</span>
				<a href="<%=path %>/DataSourceSelectAll?pageNumber=${page.pageNumber - 1}" style="display: ${page.pageNumber - 1 != 0 ? '' : 'none' }">上一页</a>
			</td>
			<td colspan="2">
				当前页: ${page.pageNumber }
			</td>
			<td colspan="2">
				<span style="display: ${page.pageNumber == page.totalPage ? '':'none'}">已是末页</span>
				<a href="<%=path %>/DataSourceSelectAll?pageNumber=${page.pageNumber + 1}" style="display: ${page.pageNumber != page.totalPage ? '':'none'}">下一页</a>
			</td>
		</tr>
	</tfoot>
</table>
</div>

<br/><br/>
<div id="sourceType" style="display:none">

</div>
</div>	

</center>
</body>

</html>