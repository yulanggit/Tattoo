<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<title>大数据计算平台</title>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<%-- <script type="text/javascript">
	var path = '<%=path%>';
</script> --%>
<script type="text/javascript" src="js/main/main.js"></script>
<link href="http://at.alicdn.com/t/font_499629_j6lh4wcwvzjgu8fr.css" rel="stylesheet" />
<link href="css/main/bootstrap.min.css" rel="stylesheet" />
<link href="css/main/bootstrap-responsive.min.css" rel="stylesheet" />
<link href="css/main/font-awesome.css" rel="stylesheet" />
<link href="css/main/style.css" rel="stylesheet" />
<link href="css/main/style_responsive.css" rel="stylesheet" />
<link href="css/main/style_default.css" rel="stylesheet" id="style_color" />
<!-- <link href="css/main/jquery.fancybox.css" rel="stylesheet" /> -->
<link rel="stylesheet" type="text/css" href="css/main/uniform.default.css" />
<!-- <link href="css/main/bootstrap-fullcalendar.css" rel="stylesheet" /> -->
<!-- <link href="css/main/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />  -->
</head>
<body class="fixed-top">
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="brand" href="">大数据计算平台</a> <a
					class="btn btn-navbar collapsed" id="main_menu_trigger"
					data-toggle="collapse" data-target=".nav-collapse"> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="arrow"></span>
				</a>
				<div id="top_menu" class="nav notify-row"></div>

				<div class="top-nav ">
					<ul class="nav pull-right top-menu">

						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <span class="username">设置</span>
						</a>
							<ul class="dropdown-menu">
								<li><a href="#"><i class="iconfont">&#xe718;</i>我的</a></li>
								<li class="divider"></li>
								<li><a href="Login.jsp"><i class="iconfont">&#xe617;</i>退出</a></li>
							</ul></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div id="container" class="row-fluid">
		<div id="sidebar" class="nav-collapse collapse">
			<div style='padding: 5px 0 0 5px'
				class="sidebar-toggler hidden-phone">
				<i class='iconfont'>&#xe66e;</i>
			</div>

			<div class="navbar-inverse">
				<form class="navbar-search visible-phone">
					<input type="text" class="search-query" placeholder="Search" />
				</form>
			</div>

			<ul class="sidebar-menu">
				<li class="has-sub"><a href="javascript:;" class=""> 
				<span class="icon-box"> <i class="iconfont">&#xe6af</i></span>数据采集 
				<span class="arrow"></span>
				</a>
					<ul class="sub">
						<li><a class="" href="DataBaseInfo.jsp">数据库连接信息</a></li>
						<li><a class="" href="LoadToHive.jsp">hive文件上传</a></li>
						<li><a class="" href="PullToHive.jsp">hive数据源拉取</a></li>
						<li><a class="" href="LoadHdfs.jsp">HDFS文件上传</a></li>
						<li><a class="" href="PullToHdfs.jsp">HDFS数据源拉取</a></li>
					</ul>
				</li>
				<li class="has-sub"><a href="javascript:;" class=""> 
				<span class="icon-box"> <i class="iconfont">&#xe67b</i></span>管理功能 
				<span class="arrow"></span>
					</a>
					<ul class="sub">
						<li><a class="" href="ManagerData1.jsp">数据源信息</a></li>
						<li><a class="" href="DataSourceGroup.jsp">数据源分组管理</a></li>
						<!-- <li><a class="" href="DataFlowManager.jsp">新建流程</a></li>
						<li><a class="" href="dataflowsearch.jsp">流程运行状态</a></li> -->
					</ul>
				</li>
				<li class="has-sub returnGoods">
					<a href="CountAnalysis.jsp" class=""> <span class="icon-box">
						<i class="iconfont">&#xe640</i></span>统计分析
					</a>
				</li>
				<li class="has-sub purchase">
					<a href="databaseSource.jsp" class=""> 
						<span class="icon-box">
							<i  class="iconfont">&#xe6a7</i>
						</span>结果展现 
					</a>
				</li>
			</ul>
		</div>

		<div id="main-content">
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<!-- <h3 class="page-title">
							管理功能<small>Management </small>
						</h3> -->
						<!-- <ul class="breadcrumb">
							<li class="pull-right search-wrap">
								<form class="hidden-phone">
									<div class="search-input-area">
										<input id=" " class="search-query" type="text"
											placeholder="Search"> <i class="iconfont">&#xe620;</i>
									</div>
								</form>
							</li>
						</ul> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		2018 &copy;大数据计算平台.
		<div class="span pull-right">
			<span class="go-top"><i class="iconfont">&#xe7a8;</i></span>
		</div>
	</div>



	<!-- <script src="js/main/jquery-1.8.3.min.js"></script> -->
	<script type="text/javascript" src="js/jquery-1.7.1.min.js">
	<script src="js/main/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="js/main/jquery.slimscroll.min.js"></script>
	<script src="js/main/fullcalendar.min.js"></script>
	<script src="js/main/bootstrap.min.js"></script>
	<script src="js/main/jquery.blockui.js"></script>
	<script src="js/main/jquery.cookie.js"></script>
	<script src="js/main/jquery.vmap.js" type="text/javascript"></script>
	<script src="js/main/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="js/main/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="js/main/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="js/main/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="js/main/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="js/main/jquery.vmap.sampledata.js" type="text/javascript"></script>
	<script src="js/main/jquery.knob.js"></script>
	<script src="js/main/jquery.flot.js"></script>
	<script src="js/main/jquery.flot.resize.js"></script>
	<script src="js/main/jquery.flot.pie.js"></script>
	<script src="js/main/jquery.flot.stack.js"></script>
	<script src="js/main/jquery.flot.crosshair.js"></script>
	<script src="js/main/jquery.peity.min.js"></script>
	<script type="text/javascript" src="js/main/jquery.uniform.min.js"></script>
	<script src="js/main/scripts.js"></script>
	<script>
	var path = '<%=path%>';
		jQuery(document).ready(
						function() {
							/* 管理功能开关 */
							for (var i = 0; i < $('.has-sub').length; i++) {
								$('.has-sub')[i].onclick = function() {
									if (this.className == 'has-sub active') {
										this.className = 'has-sub';
									} else {
										this.className = 'has-sub active';
									}
								};
							}

							/* 用户权限判断 */
							/* getDataList(); */
							if (!JSON.parse(window.sessionStorage.getItem('userMsg'))) {							
								window.location.href = "/Tattoo/Login.jsp";
							} 
						});
	</script>
</body>
</html>
