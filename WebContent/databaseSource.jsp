<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>结果展现</title>
<link rel="stylesheet" type="text/css" href="css/index.css" >
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/database.js"></script>
<script type="text/javascript" src="js/show.js"></script>
<script type="text/javascript" src="js/echarts.min.js"></script>
<%@include file="./main.jsp" %>
</head>
<body>

<!-- <!-- <span>数据库类型：</span>
<select name="databaseType">
	<option value="mysql">mysql</option>
	<option value="oracle">oracle<option>
</select><br />
<span>主机名称：</span><input type="text" name="hostName" /><br />
<span>端口号：</span><input type="text" name="port" /><br />
<span>用户名：</span><input type="text" name="user" /><br />
<span>密码：</span><input type="password" name="password" /><br />
<input type="button" value="测试连接" class="getDatabase" /><br />
<br/>
<div class="databaseList"></div>
<br />
<div class="dataTableList"></div>
<br/> --> 
<div class="div" style="position:fixed;left:300px;top:100px;z-index:1000;height:50px">
<div class="div1">
<input class="showdatabase" type="button" value="已完成的流程" />
<br/>
<div class="Hivedatabase"></div>
<div class="tableList" style="display:none;"></div>
<br/>
<div class="tableInfo" style=" display:none;height:40px;width:370px;overflow: auto;"></div>
<div class="tableData"  style="display:none;height:50px;width:370px;overflow: auto;"></div>
	<select class="type" style="display:none;">
		<option value="null">选择图表类型</option>
		<option value="bar">条形图</option>
		<option value="line">折线图</option>
		<option value="pie1">饼形图</option>
	</select>
	<div class="typeOption">
		<div class="bar-line bar line">
			
		</div>
		<div class="pie1" >

		</div>
	</div>
	<input type="button" id="btn" value="生成图表" style="display:none;"/>
</div>

<div class="div2">

	<!-- 指定图表生成区域 -->
	<div id="main" style="width:450px; height: 550px "></div>
</div>
</div>
</body>
</html>