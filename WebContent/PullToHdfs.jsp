<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>数据库拉取</title>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/PullToHdfs.js"></script>
<%@ include file="./main.jsp" %>
</head>
<body>

<div style="position:fixed;left:300px;top:80px;z-index:1000;height:50px;">
<h4>新建数据源</h4>
<span>名 &nbsp;&nbsp;&nbsp;称:</span><input type="text" name="sourcename" placeholder="数据源命名"/><br/>
<span>分组名:</span>
	<select id="selectGroup" name="group_name"></select> 

<div id="showInfo" style="display: none">
	<span>数据库:</span>
	<select id='database'></select><br/>
	<span>数据表:</span>
	<select id='table'></select><br /><br/>
	<span>自定义路径:</span>
	<input type="text" name="zdy" placeholder="例:/bigdata" />
	<span>上传目标：</span>
	<select name="targetDir" class="space">
		<option value="commonSpace">公共空间</option>
		<option value="ownSpace">个人空间</option>
	</select><br/>
	<span>单表导入/sql导入:</span>
	<select id='import'>
	<option value="Single" class="1">单表导入</option>
	<option value="Sql" class="2">Sql导入</option>
	</select><br />
	<div id="SingleTable" style="display :none">
		<input type="button" class="import" value="导入">
	</div>
	
	<div id="SqlTable" style="display :none">
	    <textarea rows="5" cols="20" id="sqlcustom" name="sqlcustom" style="width :300px " placeholder="自定义sql语句 :例：select * from data_source" ></textarea><br/>
		<input type="button" class="sqlimport" value="导入"/>
	</div>

</div>
</div>

</body>
</html>