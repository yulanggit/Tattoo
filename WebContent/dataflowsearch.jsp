<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>流程运行状态</title>
<!-- <link href="css/dataflowsearch.css" rel="stylesheet" /> -->
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/dataflowsearch.js"></script>
<%@ include file="./main.jsp"%>
</head>
<body>
	<div style='position: fixed; left: 300px; top: 100px; z-index：1000; height: 50px;'>
		<table class="tbodyList table table-striped table-bordered table-advance table-hover">
			<tr>
				<th><span class="hidden-phone">id</span></th>
				<th><span class="hidden-phone">用户</span></th>
				<th><span class="hidden-phone">hive数据库</span></th>
				<th><span class="hidden-phone">执行sql语句</span></th>
				<th><span class="hidden-phone">结果文件路径</span></th>
				<th><span class="hidden-phone">创建时间</span></th>
				<th><span class="hidden-phone">运行状态</span></th>
				<th><span class="hidden-phone">修改时间</span></th>
			</tr>
			<tbody id="dataflowstatus" class='tbodyList'>
										
		</tbody>
		</table>
	</div>
</body>
</html>