<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>数据源管理</title>
<link href="css/main/dialog.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/ManagerData1.js"></script>
<script type="text/javascript" src="js/main/dialog.js"></script>
<script type="text/javascript">
	var resultBl = "<%=request.getParameter("resultBl")%>";
	if(resultBl!='null'){
		if(resultBl=='true'){
			alert("编辑成功！")
		}else{
			alert("编辑失败！")
		}
	}
</script>
<%@ include file="./main.jsp"%>

</head>
<body>
	<div class='widget-body' style='position: fixed; left: 300px; top: 100px; z-index：1000; height: 50px;'>
		<table class="tbodyList table table-striped table-bordered table-advance table-hover">
		<thead>
			<tr>
				<th><span class="hidden-phone">id</span></th>
				<th><span class="hidden-phone">数据源名称</span></th>
				<th><span class="hidden-phone">数据源类型</span></th>
				<th><span class="hidden-phone">HDFS路径</span></th>
				<th><span class="hidden-phone">hive表名称</span></th>
				<th><span class="hidden-phone">hive表结构</span></th>
				<th><span class="hidden-phone">关联用户ID</span></th>
				<th><span class="hidden-phone">关联分组ID</span></th>
				<th><span class="hidden-phone">创建时间</span></th>
				<th><span class="hidden-phone">操作</span></th>
			</tr>
		</thead>
		<tbody class='DataInfoRecord tbodyList'>
										
		</tbody>
		</table>
	</div>
</body>
</html>