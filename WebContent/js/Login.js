$(function() {

	/* 点击确定判断 */
	$("input[type='button']").click(function() {
		var userName = $("input[name='username']").val();
		var password = $("input[name='password']").val();
		if (userName) {
			if (password) {
				getAjax();
			} else {
				alert('请您完善密码')
			}
		} else {
			alert('请您完善用户名')
		}
	});
	/* 登录请求ajax */
	function getAjax() {
		var username = $("input[name='username']").val();
		var password = $("input[name='password']").val();
		$.ajax({
			url : "LoginServlet",
			type : "post",
			data : {
				username : username,
				password : password
			},
			dataType : "json",
			success : function(data) {
				console.log(data);
				if (data == null) {
					alert("账号或密码错误")
				} else if (data.status == '200') {
					window.sessionStorage.setItem('userMsg', JSON.stringify(data.data[0]));
					window.location.href = "main.jsp";
				} else {
					alert("没有登陆权限")
				}
			},
			error : function(data) {
				alert("登陆失败")
			}
		})
	}

})
