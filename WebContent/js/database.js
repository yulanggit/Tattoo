$(function(){
	
	var userMsg="";
	var user="";
	
	/**
	 * 根据用户获得所属库
	 */
	$(".showdatabase").click(function() {
		//从session中获取信息，user_name作为表名
		userMsg=window.sessionStorage.getItem('userMsg');
		console.log(JSON.parse(userMsg));
		user=JSON.parse(userMsg);
		console.log(user);
		var HdatabaseN=user.user_name;
		//var id=user.id;
		console.log(HdatabaseN);
		var method = "showHivedatabase";
		$.ajax({
			url : "DataBaseServlet",
			type : "post",
			data : {
				method:method,
				HdatabaseN:HdatabaseN
			},
			//dataType : "json",
			success : function(data) {
				console.log(data);
				var select = $("<select class='Hdatabase' name='HdatabaseName'></select>");
				
					select.append("<option value='" + data + "'>" + data + "</option>");
				
				$(".Hivedatabase").html("");
				$(".Hivedatabase").append(select);
			}
		})
	})
	/***
	 * 显示库中的所有表
	 */
	$(".Hivedatabase").on("click",".Hdatabase",function(){
		
		$(".tableList").css('display','block');
		$(".type").css('display','block');
		$("#btn").css('display','block');
		var method = "showtableList";
		var id=user.id;
		console.log(id);
		var Hivedatabase = $(this).val();
		$.ajax({
			url : "DataBaseServlet",
			type : "post",
			data : {
				method : method,
				Hivedatabase : Hivedatabase,
				id : id
				
			},
			dataType : 'json',
			success : function(data) {
				if(data.length != 0){
					/*var content = $(".tableList");
					content.html("");
					for(index in data){
						var tableName = data[index];
						content.append("<div><span>" + tableName + "</span><input class='info' type='button' value='结构信息' data-name='" + tableName + "' /><input class='data' type='button' value='预览数据' data-name='" + tableName + "' /></div>");
					}*/
					
					
					var select = $("<select class='tableName'>"+ tableName +"</select>");
					var content = $(".tableList");
					for(index in data){
						var tableName = data[index];
						select.append("<option value='" + tableName + "'>" + tableName + "</option>");
						//console.log(tableName);
						content.append(select);
						content.append("<input class='info' type='button' value='结构信息' data-name='" + tableName + "' /><input class='data' type='button' value='预览数据' data-name='" + tableName + "' /></div>");
					}
					content.html("");
					$(".tableInfo").html("");
					$(".tableData").html("");
					content.append(select);
					content.append("<br/><input class='info' type='button' value='结构信息' data-name='" + tableName + "' />&nbsp;&nbsp;<input class='data' type='button' value='预览数据' data-name='" + tableName + "' /></div>");
					
					//$(".tableList").append(content);
				}else{
					var content = $(".tableList");
					content.html("");
					alert("该数据元下无数据表");
				}
				
			}
		})
		
	})
	/**
	 * 显示表结构
	 */
//	$(".tableList").on("change",".tableName",function(){
//		var tableName1= $("select[name='tableName']").val();
//		var tableName = tableName1;
//		console.log(tableName);
	
	$(document).on("click",".info",function(){
		// 通过data("xxx")方法可以获得当前元素通过data-xxx属性定义的值
		$(".tableInfo").css('display','block');
		var tableName = $(".tableName").val();
		//console.log(tableName);
		var HdatabaseName=$("select[name='HdatabaseName']").val();
		var method="tableInfo";
		// 通过触发事件元素与需要获取信息的元素之间的层级关系进行查找
		// siblings能够获取到同级元素的集合，也可以直接传入选择器作为参数，如siblings(".className")，会返回同级中匹配的元素
		//alert($(this).siblings().eq(0).html());
		$.ajax({
			url : "DataBaseServlet",
			type : "post",
			data : {
				tableName : tableName,
				HdatabaseName : HdatabaseName,
				method : method
			},
			dataType : "json",
			success:function(data){
				var content = $(".tableInfo");
				// 保证指定区域只显示当前表信息，添加信息前先清空
				content.html("");
				for(index in data){
					content.append("<div>" + data[index] + "</div>")
				}
			}
		})
	})
	/**
	 * 显示表预览信息
	 */
	$(document).on("click",".data",function(){
		$(".tableData").css('display','block');
		var tableName = $(".tableName").val();
		var HdatabaseName=$("select[name='HdatabaseName']").val();
		console.log(tableName);
		console.log(HdatabaseName);
		var method="tableData";
		$.ajax({
			url : "DataBaseServlet",
			type : "post",
			data : {
				tableName : tableName,
				HdatabaseName : HdatabaseName,
				method : method
			},
			dataType : "json",
			success:function(data){
				var content = $(".tableData");
				// 保证指定区域只显示当前表信息，添加信息前先清空
				content.html("");
				for(index in data){
					content.append("<div>" + data[index] + "</div>")
				}
			}
		})
	})
	//})
})