$(function(){
	$(".add").click(function(){
		var column=$(".column").html();
		$(".columnInfo").append("<div>"+column+"<br /><br /></div>");
	})
	$(document).on("click",".delete",function(){
		$(this).parent().remove();
	})

	
	var userMsg=window.sessionStorage.getItem('userMsg');
	var user=JSON.parse(userMsg);
	var userName=user.user_name;
	var id=user.id;
	$("#userId").val(id);
	$("#userName").val(userName);
	
	var content = $("#selectGroup");
	
	$.ajax({
		url:"DatascGroupServlet",
		type:"post",
		data:{
			userName:userName,
			id:id
		},
		dataType:"json",
		success:function(data){
		    content.html("");
			for(index in data.data){
				content.append("<option value='"+ data.data[index].id +"'>" + data.data[index].group_name + "</ option>");
			}
		}
	})
	
})