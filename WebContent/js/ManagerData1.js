$(function() {
	if (!(JSON.parse(window.sessionStorage.getItem('userMsg')))) {
		window.location.href = "/Tattoo/Login.jsp";
	}
	var userMsg=window.sessionStorage.getItem('userMsg');
	var user=JSON.parse(userMsg);
	var userName=user.user_name;
	var userId=user.id;
	$.ajax({
		url : "ManagerDataServlet",
		type : "post",
		data : {
			method : "selectAll",
			userId:userId
		},
		datatype : "json",
		success : function(data) {
			var content = $(".DataInfoRecord");
			console.log(typeof(data));
			content.html("");
			for (index in JSON.parse(data).data) {
				content.append("<tr>")
				content.append("<th><span class='hidden-phone'>"+JSON.parse(data).data[index].id+"</span></th>")
				content.append("<th><span class='hidden-phone'>"+JSON.parse(data).data[index].source_name+"</span></th>")
				content.append("<th><span class='hidden-phone'>"+JSON.parse(data).data[index].source_type+"</span></th>")
				content.append("<th><span class='hidden-phone'>"+JSON.parse(data).data[index].hdfs_path+"</span></th>")
				content.append("<th><span class='hidden-phone bt'>"+JSON.parse(data).data[index].hive_table+"</span></th>")
				content.append("<th><span class='hidden-phone'>"+JSON.parse(data).data[index].table_column+"</span></th>")
				content.append("<th><span class='hidden-phone'>"+JSON.parse(data).data[index].user_id+"</span></th>")
				content.append("<th><span class='hidden-phone'>"+JSON.parse(data).data[index].group_id+"</span></th>")
				content.append("<th><span class='hidden-phone'>"+JSON.parse(data).data[index].create_date+"</span></th>")
				content.append("<th><span class='hidden-phone'><a class='delete' data='"+JSON.parse(data).data[index].id+"'>删除</a>  <a class='edit' data='"+JSON.parse(data).data[index].id+"'>编辑</a></span></th>")
				content.append("</tr>");
			}
		},
		error : function(data) {
			alert("错误")
		}
	})
	

	$(document).on("click", ".bt", function(e){
		var table_name= e.target.innerHTML
		$.ajax({
			url : "ManagerDataServlet",
			type : "post",
			data : {
				method : "tableinfo",
				userId:userId,
				userName:userName,
				table_name:table_name
				
			},
			datatype : "json",
			success : function(data) {
				data = JSON.parse(data);
				console.log(data.data);
				
				var str='<tr><th>key</th><th>value</th></tr>';
				
				data.data.forEach((val,key)=>{
					str+='<tr><td>'+key+'</td><td>'+val+'</td></tr>'
				})
				var content='<table class="table">'+str+'</table>';
				
				pop({
					width:500,//提示窗口的宽度
					height:200,//提示窗口的高度
					title:e.target.innerHTML,//提示窗口的标题
					//content:'这是提示信息'//提示窗口的内容
					content:content
				});
				
			},
			error : function(data) {
				alert("错误")
			}
		})
		console.log(e.target.innerHTML)
		
	})
	
	// 编辑数据源文件
	$(document).on("click", ".edit", function(e) {
		var source_id=e.target.getAttribute('data');
		var link="/Tattoo/HiveDataManager.jsp"+'?source_id='+source_id;
		window.location.href=link;
	})
	
	// 删除数据源文件
	$(document).on("click", ".delete", function(e) {
		console.log(e.target.getAttribute('data'));
		var source_id = e.target.getAttribute('data');
		alert(source_id);
		$.ajax({
			url:"ManagerDataServlet",
			type:"post",
			async:false,
			data:{
				method:"delete",
				source_id:source_id,
				userName:userName,
				userId:userId
			},
			datatype:"text",
			success:function(data){
				console.log(data)
				if(data!=-1){
					alert("删除成功")
					window.location.reload();
				}else{
					alert("删除失败");
				}
			}
		})
	})
	
	

})