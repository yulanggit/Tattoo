$(function(){
	$(".bar-line").on("click",".add",function(){
		var div = $("<div></div>");
		console.log(div);
		div.append("<span>添加项:</span>");
			
		//取到var select2 = $("<select class='y'></select>");这个元素整体
	    	var select=$(".y").eq(0)[0].outerHTML;
	    	console.log(select);
			div.append(select);
			//添加删除事件
		    div.append("&nbsp;&nbsp;<span class='delete'>删除</span>");
		
		    $(".bar-line").append(div);
	})
	//选择图表类型，根据表信息获取字段作为项轴
	$(".type").val("null");
	var type = "null";
	$(".bar-line").on("click",".delete",function(){
		$(this).parent().remove();
	})
	$(".type").change(function(){
		var type = $(".type").val();
		if(type == "null"){
			$(".typeOption").find("div").hide();
		}
		$(".typeOption").find("." + type).show();
		$(".typeOption").find("." + type).siblings().hide();
		var method = "showgetData";
		var tableName = $(".tableName").val();
		var HdatabaseName=$("select[name='HdatabaseName']").val();
		
		$.ajax({
			url:"ShowServlet",
			type:"post",
			data:{
				method:method,
				tableName : tableName,
				HdatabaseName : HdatabaseName,
				type:type
			},
			dataType:'json',
			success:function(data){
				//若为折线图或柱形图
				if("bar"==type||"line"==type){
				var select1 = $("<select class='x'></select>");
				var select2 = $("<select class='y'></select>");
				
				for(var index in data){
					
					select1.append("<option value='" + data[index] + "'>" + data[index] + "</option><br/>") ;
					select2.append("<option value='" + data[index] + "'>" + data[index] + "</option>") ;
				    
				}
				
				$(".bar-line").html("");
				$(".bar-line").append("<span>横 轴:</span>");
				$(".bar-line").append(select1);
				$(".bar-line").append("<br/>");
				$(".bar-line").append("<span>纵 轴:</span>");
				$(".bar-line").append(select2);
				$(".bar-line").append("<input type='button' class='add' value='添加项'/>");
				//若为饼形图
				}else if("pie1"==type){
					var select3 = $("<select class='n'></select>");
					var select4 = $("<select class='v'></select>");
					
					for(var index in data){
						select3.append("<span>描述:</span><option value='" + data[index] + "'>" + data[index] + "</option>") ;
						select4.append("<span>数据:</span><option value='" + data[index] + "'>" + data[index] + "</option>") ;
					
					}
					$(".pie1").html("");
					$(".pie1").append("<span>描述:</span>");
					$(".pie1").append(select3);
					$(".pie1").append("<br/>");
					$(".pie1").append("<span>数据:</span>");
					$(".pie1").append(select4);
					
					
				}
				
			}
		})
	})
	

	var option = {};
	$("#btn").click(function(){
		
		var type=$(".type").val();
		console.log(type);
		var method = "showChart";
		var tableName = $(".tableName").val();
		var HdatabaseName=$("select[name='HdatabaseName']").val();
		console.log(tableName);
		
		console.log(HdatabaseName);
		if(type == "null"){
			alert("请选择图表类型");
			return;
		}else{
			//柱形图，折线图
			if(type == "line" || type == "bar"){
				var xAxis = $(".x").val();
				//yAxis是一个数组
				var yAxis = [];
				$(".y").map(function(){
					//添加每次的值
					yAxis.push($(this).val());
				
				})
				$.ajax({
					url : 'ShowServlet',
					type : 'post',
					traditional : true,  //代表ajax可传输一个数组
					data : {
						method:method,
						type : type,
						tableName : tableName,
						HdatabaseName:HdatabaseName,
						xAxis : xAxis,
						yAxis : yAxis
					},
					dataType : 'json',
					success : function(data){
					
						//此时传过来的数据是个map集合
						var xData = data.xData;
						var seriesData = [];
						//循环data，当key不为xData时，往seriesData中push数据
						for(key in data){
							if(key != "xData"){
								seriesData.push({
									data : data[key],
									type : type
								})
							}
						}
						var yData = data.yData;
						option={};
						option = {
							xAxis : {
								type: 'category',
								data : xData
								
							},
							yAxis : {
								type : 'value'
								
							},
							//方便数组push数据
							series : seriesData
						};
						$("#main").remove();
						$(".div2").html("<div id='main' style='width:450px;height:550px'></div>")
						echarts.init(document.getElementById("main")).setOption(option);
						
						
					}
				})
				//饼形图
			}else if(type=="pie1"){
		     var name2 = $(".n").val();
              console.log(name2);
		       var value = $(".v").val();
                console.log(value);
				$.ajax({
					url : 'ShowServlet',
					type : 'post',
					
					data : {
						method:method,
						type : type,
						HdatabaseName:HdatabaseName,
						tableName : tableName,
						name2 : name2,
						value : value
					},
					dataType : 'json',
					success : function(data){
						//alert(data);
						var legendData = data.legendData;
						var innerData = data.innerData;
						var seriesData = [];
						for(var index in innerData){
						seriesData.push({name:innerData[index],value:legendData[index]});
						}
						option={};
						option = {
							    title : {
							       // text: '某站点用户访问来源',
							        x:'center'
							    },
							    tooltip : {
							        trigger: 'item',
							        formatter: "{a} <br/>{b} : {c} ({d}%)"
							    },
							    legend: {
							        orient: 'vertical',
							        left: 'left',
							        data: innerData
							    },
							    series : [
							        {
							            name: '数据信息',
							            type: 'pie',
							            radius : '55%',
							            center: ['50%', '60%'],
							            data:seriesData,
							            itemStyle: {
							                emphasis: {
							
							                	
							                	shadowBlur: 10,
							                    shadowOffsetX: 0,
							                    shadowColor: 'rgba(0, 0, 0, 0.5)'
							                }
							            }
							        }
							    ]
							};
						console.log(option);
					
						$("#main").remove();
						$(".div2").html("<div id='main' style='width:450px;height:550px'></div>")
						echarts.init(document.getElementById("main")).setOption(option);
				
						
					}
				})
			}
		}
		
	})
})