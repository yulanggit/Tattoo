$(function(){
	var userMsg=window.sessionStorage.getItem('userMsg');
	var user=JSON.parse(userMsg);
	var userName=user.user_name;
	var id=user.id;
	var content = $("#selectGroup");
	var content1 = $("#NewGroup");
	$.ajax({
		url:"DatascGroupServlet",
		type:"post",
		data:{
			userName:userName,
			id:id
		},
		dataType:"json",
		success:function(data){
		    content.html("");
			for(index in data.data){
				content.append("<option value='"+ data.data[index].id +"'>" + data.data[index].group_name + "</ option>");
			}
		}
	})
	
	// 新增分组
	$("#newGroup").click(function(){
		content1.html("");
		content1.append("<span> 新建分组名  </span>")
		content1.append("<input type='text' name = 'newgroupName' ><br>")
		content1.append("<input  class='button' id='submitnewgroup' type='button' value='提交' >")
	})
	
	// 修改分组
	$("#updateGroup").click(function(){
		content1.html("");
		content1.append("<span> 新分组名  </span>")
		content1.append("<input type='text' name = 'UpdategroupName' ><br>")
		content1.append("<input  class='button' id='submitupdategroup' type='button' value='提交' >")
	})
	
	// 删除分组
	$("#deleteGroup").click(function(){
		// alert($("#selectGroup option:selected").text())
		var group_id = $("#selectGroup").val();
		var group_name = $("#selectGroup option:selected").text();
		$.ajax({
			url:"NewGroupServlet",
			type:"post",
			data:{
				method:"deleteGroup",
				group_id:group_id,
				group_name:group_name,
				userName:userName,
				id:id
			},
			dataType:"json",
			success:function(data){
				console.log(data)
				if(data.status==200){
					alert("删除成功")
					window.location.reload()
				}else{
					alert("删除失败")
				}
			}
		})
	})
	
	//新增分组
	$(document).on("click", "#submitnewgroup", function() {
		var newgroupName = $("input[name='newgroupName']").val();
		// alert(newgroupName);
		$.ajax({
			url:"NewGroupServlet",
			type:"post",
			data:{
				method:"newGroup",
				id:id,
				userName:userName,
				newgroupName:newgroupName
			},
			dataType:"json",
			success:function(data){
				console.log(data)
				if(data.status==200){
					alert("添加成功")
					window.location.reload()
				}else{
					alert("添加失败")
				}
			}
		})
	})
	
	// 修改分组
	$(document).on("click", "#submitupdategroup", function() {
		var group_id = $("#selectGroup").val();
		var group_name = $("#selectGroup option:selected").text();
		var UpdategroupName = $("input[name='UpdategroupName']").val();
		alert(UpdategroupName);
		$.ajax({
			url:"NewGroupServlet",
			type:"post",
			data:{
				method:"updateGroup",
				id:id,
				userName:userName,
				group_id:group_id,
				group_name:group_name,
				UpdategroupName:UpdategroupName
			},
			dataType:"json",
			success:function(data){
				console.log(data)
				if(data.status==200){
					alert("修改成功")
					window.location.reload()
				}else{
					alert("修改失败")
				}
			}
		})
	})
})