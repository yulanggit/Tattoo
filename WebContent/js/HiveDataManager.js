$(function(){
	var userMsg=window.sessionStorage.getItem('userMsg');
	var user=JSON.parse(userMsg);
	var userName=user.user_name;
	var id=user.id;
	var tableName = "";
	var hdfs_path = "";
	$("#userId").val(id);
	$("#userName").val(userName);
	$.ajax({
		url:"HiveDataManagerServlet",
		type:"post",
		async:false,
		data:{
			method:"first",
			source_id:source_id
		},
		dataType:"json",
		success:function(data){
			tableName = data.data[1];
			hdfs_path = data.data[2];
			$("#tableName").val(tableName);
			$("#hdfs_path").val(hdfs_path);
			if(data.status==200){
				if(data.data[0] == "hive"){
					document.getElementById("hive").style.display="";
					document.getElementById("hdfs").style.display="none";
				}else{
					document.getElementById("hive").style.display="none";
					document.getElementById("hdfs").style.display="";
				}
			}
		}
	})
	
	// hive数据上传方式选择
	$("#parenthive").change(function(){
		var value = $(this).val();
		if(value==1){
			document.getElementById("hivefileinput").style.display="";
			document.getElementById("hivetableinput").style.display="none";
		}else if(value==2){
			document.getElementById("hivefileinput").style.display="none";
			document.getElementById("hivetableinput").style.display="";
			var select1=$("#database");
			//请求数据源
			$.ajax({
				url : "PullToHiveServlet",
				type : "post",
				data : {
					method : "obainDB"
				},
				dataType :"json",
				success : function(data){
					console.log(data);
					for(index in data){
						select1.append("<option value='"+ data[index].id +"'>" + data[index].sourcename + "</ option>");
					}
				}
			})	
		}else{
			document.getElementById("hivefileinput").style.display="none";
			document.getElementById("hivetableinput").style.display="none";
		}
	})
	
	// hdfs数据上传方式选择
	$("#parenthive1").change(function(){
		var value = $(this).val();
		if(value==1){
			document.getElementById("hdfsfileinput").style.display="";
			document.getElementById("hdfstableinput").style.display="none";
		}else if(value==2){
			document.getElementById("hdfsfileinput").style.display="none";
			document.getElementById("hdfstableinput").style.display="";
			var select1=$("#database1");
			//请求数据源
			$.ajax({
				url : "PullToHiveServlet",
				type : "post",
				data : {
					method : "obainDB"
				},
				dataType :"json",
				success : function(data){
					console.log(data);
					for(index in data){
						select1.append("<option value='"+ data[index].id +"'>" + data[index].sourcename + "</ option>");
					}
				}
			})	
		}else{
			document.getElementById("hdfsfileinput").style.display="none";
			document.getElementById("hdfstableinput").style.display="none";
		}
	})
	
	// 展示数据库中的表hive
	$(document).on("change","#database",function(){
		 database=$(this).val();
		 var select2=$("#table");
		 var method="getdatabase";
		 $.ajax({
			url : "PullToHiveServlet",
			type : "post",
			async:false,
			data : {
				method : method,
				database : database
			},
			dataType :"json",
			success : function(data){
				if(data.length != 0){
					for(index in data){
						select2.append("<option value='" + data[index] + "'>" + data[index] + "</option>");
					}
				}
			}
		}) 
	})
	
	// 展示数据库中的表hdfs
	$(document).on("change","#database1",function(){
		 database=$(this).val();
		 var select2=$("#table1");
		 var method="getdatabase";
		 $.ajax({
			url : "PullToHiveServlet",
			type : "post",
			async:false,
			data : {
				method : method,
				database : database
			},
			dataType :"json",
			success : function(data){
				if(data.length != 0){
					for(index in data){
						select2.append("<option value='" + data[index] + "'>" + data[index] + "</option>");
					}
				}
			}
		}) 
	})
	
	// sql或单表导入hive
	$(document).on("change","#import",function(){
		importType=$(this).val();
		if(importType=="Single"){
			document.getElementById("SingleTable").style.display="";
			document.getElementById("SqlTable").style.display="none";
		}else{
			document.getElementById("SingleTable").style.display="none";
			document.getElementById("SqlTable").style.display="";
		}
	})
	
	// sql或单表导入hdfs
	$(document).on("change","#import1",function(){
		importType=$(this).val();
		if(importType=="Single"){
			document.getElementById("SingleTable1").style.display="";
			document.getElementById("SqlTable1").style.display="none";
		}else{
			document.getElementById("SingleTable1").style.display="none";
			document.getElementById("SqlTable1").style.display="";
		}
	})
	
	// 单表导入 hive
	$(".import").click(function(){
		var method = "SingleImport2";
		var table = $("#table").val();
		var database = $("#database").val();
		var overwrite = $("input[type='checkbox']").is(':checked');//是否覆盖导入
		$.ajax({
			url : "PullToHiveServlet",
			async:false,
			type : "post",
			data : {
				method : method,
				database : database,
				tableName : tableName,
				table : table,
				overwrite : overwrite,
				userName : userName

			},
			dataType : "json",
			success : function(data){
				console.log(data)
				if(data.status==200){
					alert("编辑成功")
				}else{
					alert("编辑失败")
				}
			}
		})
	})
	
	// 单表导入 hdfs
	$(".import1").click(function(){
		var method = "SingleImport2";
		var table1 = $("#table1").val();
		var database1 = $("#database1").val();
		var overwrite1 = $("input[name='overwrite1']").is(':checked');//是否覆盖导入
		$.ajax({
			url : "PullToHdfsServlet",
			async:false,
			type : "post",
			data : {
				method : method,
				database : database1,
				hdfs_path : hdfs_path,
				table : table1,
				overwrite : overwrite1,
				userName : userName

			},
			dataType : "json",
			success : function(data){
				console.log(data)
				if(data.status==200){
					alert("编辑成功")
				}else{
					alert("编辑失败")
				}
			}
		})
	})
	
	// sql导入hive
	$(".sqlimport").click(function(){
		var method = "sqlImport2";
		// var table = $("#table").val();
		var sqlcustom=$("#sqlcustom").val();//sql语句
		var database = $("#database").val();
		var overwrite = $("input[type='checkbox']").is(':checked');//是否覆盖导入
		$.ajax({
			url : "PullToHiveServlet",
			async:false,
			type : "post",
			data : {
				method : method,
				database : database,
				tableName : tableName,
				sqlcustom : sqlcustom,
				overwrite : overwrite,
				userName : userName

			},
			dataType : "json",
			success : function(data){
				console.log(data)
				if(data.status==200){
					alert("编辑成功")
				}else{
					alert("编辑失败")
				}
			}
		})
	})
	
	// sql导入hdfs
	$(".sqlimport1").click(function(){
		var method = "sqlImport2";
		// var table = $("#table").val();
		var sqlcustom=$("#sqlcustom1").val();//sql语句
		var database = $("#database1").val();
		var table = $("#table1").val();
		var overwrite = $("input[name='overwrite2']").is(':checked');//是否覆盖导入
		
		$.ajax({
			url : "PullToHdfsServlet",
			async:false,
			type : "post",
			data : {
				method : method,
				database : database,
				table:table,
				hdfs_path : hdfs_path,
				sqlcustom : sqlcustom,
				overwrite : overwrite,
				userName : userName
			},
			dataType : "json",
			success : function(data){
				console.log(data)
				if(data.status==200){
					alert("编辑成功")
				}else{
					alert("编辑失败")
				}
			}
		})
	})
	
})