$(function(){
	//表名
	var table;
	//库id
	var database;
		document.getElementById("showInfo").style.display="";
		var method="obainDB";
		var select1=$("#database");
		//请求数据源
		$.ajax({
			url : "PullToHiveServlet",
			type : "post",
			async:false,
			data : {
				method : method
			},
			dataType :"json",
			success : function(data){
				select1.html("");
				select1.append("<option value=''>" + "请选择数据库" + "</ option>");
				for(index in data){
					select1.append("<option value='"+ data[index].id +"'>" + data[index].sourcename + "</ option>");
				}
				
			}
		})	
	$("#showInfo").on("change","#database",function(){
			 database=$(this).val();
			 var select2=$("#table");
			 
			 var method="getdatabase";
			$.ajax({
				url : "PullToHiveServlet",
				type : "post",
				async:false,
				data : {
					method : method,
					database : database
				},
				dataType :"json",
				success : function(data){
					select2.html("");
					select2.append("<option value=''>" + "请选择数据表" + "</ option>");
					if(data.length != 0){
						for(index in data){
							select2.append("<option value='" + data[index] + "'>" + data[index] + "</option>");
						}
					}
				}
			}) 
	})
	
	
	
	$("#showInfo").on("change","#table",function(){
		table=$(this).val();
	})
	
	//获取导入类型
	$("#import").on("click",".1",function(){
		//单表导入显示，sql导入隐藏
		document.getElementById("SingleTable").style.display="";
		document.getElementById("SqlTable").style.display="none";
	})
	$("#import").on("click",".2",function(){
		//单表导入隐藏，sql导入显示
		
		document.getElementById("SingleTable").style.display="none";
		document.getElementById("SqlTable").style.display="";
	})
	

	var userMsg=window.sessionStorage.getItem('userMsg');
	var user=JSON.parse(userMsg);
	var userName=user.user_name;
	var id=user.id;
	$("#userId").val(id);
	$("#userName").val(userName);
	
	var content = $("#selectGroup");
	
	$.ajax({
		url:"DatascGroupServlet",
		type:"post",
		async:false,
		data:{
			userName:userName,
			id:id
		},
		dataType:"json",
		success:function(data){
		    content.html("");
			for(index in data.data){
				content.append("<option value='"+ data.data[index].id +"'>" + data.data[index].group_name + "</ option>");
			}
		}
	})
	

	$(".import").click(function(){
		var method="SingleImport";
		var group_id = $("#selectGroup").val();//分组id
		var group_name = $("#selectGroup option:selected").text();//分组名
		var sourcename=$("input[name='sourcename']").val();//数据源命名
		var tablename=$("input[name='tablename']").val();//自定义表名
		
		$.ajax({
			url : "PullToHiveServlet",
			type : "post",
			async:false,
			data :{
				method : method,
				group_id : group_id,
				group_name : group_name,
				sourcename : sourcename,
				table : table,
				database : database,
				userName : userName,
				id : id,
				tablename : tablename
			},
			dataType : "json",
			success:function(data){
				if(data.status==200){
					alert("导入成功");
				}else {
					alert("导入失败");
				}
			}
			
		})
	})
	$(".sqlimport").click(function(){
		var method="sqlImport";
		var group_id = $("#selectGroup").val();//分组id
		var group_name = $("#selectGroup option:selected").text();//分组名
		var sourcename=$("input[name='sourcename']").val();//数据源命名
		var sqlcustom=$("#sqlcustom").val();//sql语句
		var tablename=$("input[name='tablename']").val();//自定义表名
		alert(tablename);
		$.ajax({
			url : "PullToHiveServlet",
			type : "post",
			async:false,
			data :{
				method : method,
				group_id : group_id,
				group_name : group_name,
				sourcename : sourcename,
				table : table,
				database : database,
				userName : userName,
				id : id,
				sqlcustom :sqlcustom,
				tablename : tablename
			},
			dataType : "json",
			success:function(data){
				if(data.status==200){
					alert("导入成功");
				}else {
					alert("导入失败");
				}
			}
			
		})
	})
	
	
})