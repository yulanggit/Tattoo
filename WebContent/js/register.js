$(function(){
	$(".register").click(function(){
		var method = "register";
		var user_name = $("input[name='user_name']").val();
		var user_pwd = $("input[name='user_pwd']").val();
		var name = $("input[name='name']").val();
		var type = $("select[name='type']").val();
		var birthday = $("input[name='birthday']").val();
		
		
		
		$.ajax({
			url : 'RegisterServlet',
			type :'post',
			data :{
				method : method,
				user_name : user_name,
				user_pwd : user_pwd,
				name : name,
				type : type,
				birthday : birthday
			},
			dataType : 'json',
			success : function(data){
				console.log(data);
				if(data){
					alert("恭喜注册成功！");
					window.location.href = "main.jsp";
				}else {
					alert("注册失败,请检查信息！");
				}
			},
			error :{
			
			}
		})
	})
	$("input[name='user_name']").keyup(function() { //失去焦点
		var namestr = $(this).val();
		/*var reg1 = /^(\d+[a-zA-Z]+|[a-zA-Z]+\d+)([0-9a-zA-Z]*)$/;
		var reg2 = /^[a-zA-Z0-9]{6,10}$/;*/
		var reg = /^[a-zA-Z][a-zA-Z0-9_]{4,15}$/;

		if (!reg.test(namestr)) {
			$(this).parent().find("span").html("必须以字母开头,允许字母数字下划线,且长度为5-16位").css("color", "red");
			return false;
		}
			$(this).parent().find("span").html("");
			return true;
	});
	$("input[name='user_pwd']").keyup(function() { //失去焦点
		var pwdstr = $(this).val();
		var reg = /^[a-zA-Z0-9]{6,100}$/;
		if (!reg.test(pwdstr)) {
			
			$(this).parent().find("span").html("长度必须大于6位").css("color", "red");
			return false;
		}
			$(this).parent().find("span").html("");
			return true;
	});
	
	
	//根据登录账号、用户名、密码是否都输入，锁定注册按钮
	/*window.onload =function()
    {
       // document.getElementById("bt").disabled=true;
        $(".listen").bind("input propertychange",function(event){
        	var listen = this.value;
        	listen = $.trim(listen);
        	this.value = listen;
        	if(listen.length == 0){
        		
        		$(".register").attr("disabled",true);
        		
        	}else{
        		
        		$(".register").removeAttr("disabled");
        	}
        	
        	
        })
    }*/
    
	
})