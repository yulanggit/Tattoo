$(function(){
	// 判断是否登陆
	if (!(JSON.parse(window.sessionStorage.getItem('userMsg')))) {
		window.location.href = "/Tattoo/Login.jsp";
	} 
	
	var userMsg = window.sessionStorage.getItem('userMsg');
	console.log(JSON.parse(userMsg));
	var user = JSON.parse(userMsg);
	var user_id = user.id;
	var user_name = user.user_name;
	//选择分析方式后，请求获取数据源、算法
	$(".getways").click(function(){
		var ways = $("select[name='ways']").val();
		//console.log(ways);
		if(ways=="MR"){
			document.getElementById("showmr").style.display="";
			document.getElementById("showhql").style.display="none";
			var method1 = "showDatabasesMR";
			var method2 = "showAlgorithm";
			var select1 = $("#database");
			
			//请求获取数据源
			$.ajax({
				url : 'CountAnalysisServlet',
				type : 'post',
				data : {
					method : method1,
					user_id : user_id,
					user_name : user_name
				},
				dataType : 'json',
				success : function(data){
					//清空前一次追加内容
					select1.html("");
					console.log(data);
					
					select1.append("<option value=''>" + "请选择数据源" + "</ option>");
					for(index in data.data){
						select1.append("<option value='"+ data.data[index].id +"'>" + data.data[index].source_name + "</ option>");
					}
					
				}
			})
			
			//开始计算	
			$(".start").click(function(){
			//获取数据源、算法、
			var resultTablename = $("input[name='resultTablename']").val(); 
			var flowName = $("input[name='flowName']").val();
			var database = $("#database").val();
			var alg = $("select[name='algorithm']").val();
			var method4 = "MRcalculation";
			console.log(database);
			console.log(alg);
			
			$.ajax({
				url : 'CountAnalysisServlet',
				async: false,
				type : 'post',
				data : {
					method : method4,
					user_id : user_id,
					resultTablename : resultTablename,
					flowName : flowName,
					database : database,
					alg : alg,
					user_name : user_name
				},
				//dataType : 'json',
				success : function(data){
					//展示计算结果    结合结果展示
					if(data){
						alert("计算完成");
						
					}
				}, 
				error : function(){
					alert("计算失败");
				}
				
			})
		})
		}else if(ways=="HQL"){
			document.getElementById("showhql").style.display ="";
			document.getElementById("showmr").style.display="none";
			
			//HQL选择数据源，同MR
			var method3 = "showDatabasesHQL";
			var select3 = $("#database1");
			$.ajax({
				url : 'CountAnalysisServlet',
				type : 'post',
				data : {
					method : method3,
					user_id : user_id,
					user_name : user_name
				},
				dataType : 'json',
				success : function(data){
					//清空前一次追加内容
					console.log(data);
					select3.html("");
					select3.append("<option value=''>" + "请选择数据源" + "</ option>");
					for(index in data.data){
						select3.append("<option value='"+ data.data[index].id +"'>" + data.data[index].hive_table + "</ option>");
					}
				}
			})
			 //显示预览信息
			$(".showTable").click(function(){
				var tableId = $("#database1").val()
				console.log(tableId)
				var methodd = "showTable1"
				alert(methodd)
				//获取字段
				$.ajax({
					url : 'CountHQLServlet',
					type : 'post',
					data : {
						tableId : tableId,
						method : methodd,
						user_id : user_id,
						user_name : user_name
					},
					dataType:"json",
					success : function(data){
						console.log(data);
						var content = $("#tableHead");
						// 保证指定区域只显示当前表信息，添加信息前先清空
						content.html("");
						for(index in data.data){
							content.append("<span>" + data.data[index] + "</span>&nbsp")
						}
					}
				})
				//获取内容
				var methodd1 = "showTable2";
				$.ajax({
					url : 'CountHQLServlet',
					type : 'post',
					data : {
						tableId : tableId,
						method : methodd1,
						user_id : user_id,
						user_name : user_name
					},
					dataType:"json",
					success : function(data){
						console.log(data);
						if(data!=null){
							var content = $("#demo2");
							// 保证指定区域只显示当前表信息，添加信息前先清空
							content.html("");
							for(index in data.data){
								content.append("<div>" + data.data[index] + "</div>")
							}
						}else{
							alert("该表为空");
						}
					}
				})	
		})
		//开始计算	
		$(".begin").click(function(){
			//存储并获取一次执行状态  
			document.getElementById("zt").style.display ="";
			var ownsql = $(".ownsql").val();
			var resultTable = $("input[name='resultTable']").val();
			var dataSourceId = $("#database1").val();
			var common = $("select[name='common']").val();
			var method5 = "HQLcalculation";
			var method6 = "flushStatus1";
			var content = $("#dataflowstatus");
			content.html("");
			$.ajax({
				url : "AnalysisHQLServlet",
				async : false,
				type : "post",
				data : {
					method : method6,
					user_id : user_id,
					ownsql : ownsql,
					resultTable : resultTable,
					dataSourceId : dataSourceId,
					user_name : user_name
				},
				dataType : "json",
				success : function(data) {
					console.log(data);
					content.html("");
					for (index in data) {
						content.append("<tr>")
						content.append("<th><span class='hidden-phone'>"+data[index].id+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].flow_name+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].source_id+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].flow_type+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].hive_sql+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].flow_status+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].result_table+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].user_id+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].update_time+"</span></th>")
						content.append("</tr>");
					}
				}
			})
			//运算执行
			$.ajax({
				url : 'AnalysisHQLServlet',
				async : false,
				type : 'post',
				data : {
					method : method5,
					user_id : user_id,
					ownsql : ownsql,
					resultTable : resultTable,
					dataSourceId : dataSourceId,
					common : common,
					user_name : user_name
				},
				dataType : 'json',
				success : function(data){
					//展示计算结果    结合结果展示
					console.log(data);
					if(data==null){
						alert("计算失败");
					}else{
						console.log(data);
						var content = $("#table");
						// 保证指定区域只显示当前表信息，添加信息前先清空
						content.html("");
						content.append("<s style='font-size: 24px;margin-left: 230px;'>计算结果</s><br />");
						for(index in data.data){
							content.append("<div>" + data.data[index] + "</div>");
						}
					}
				},
				error:function(){
					alert("请求失败！");
				}
			})
			
			//在此发起一次请求，获取执行状态
			var method7 = "flushStatus2";
			document.getElementById("zt").style.display ="";
			content.html("");
			$.ajax({
				url : "AnalysisHQLServlet",
				async : false,
				type : "post",
				data : {
					method : method7,
					user_id : user_id,
					ownsql : ownsql,
					resultTable : resultTable,
					dataSourceId : dataSourceId,
					user_name : user_name
					
				},
				dataType : "json",
				success : function(data) {
					console.log(data);
					content.html("");
					for (index in data) {
						content.append("<tr>")
						content.append("<th><span class='hidden-phone'>"+data[index].id+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].flow_name+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].source_id+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].flow_type+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].hive_sql+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].flow_status+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].result_table+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].user_id+"</span></th>")
						content.append("<th><span class='hidden-phone'>"+data[index].update_time+"</span></th>")
						content.append("</tr>");
					}
				}
			})
		})
		//查看echarts图
		$(".echarts").click(function(){
			var link="/Tattoo/databaseSource.jsp";
			window.location.href=link;
		})
		
		}
	})
})


